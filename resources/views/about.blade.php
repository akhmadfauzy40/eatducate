@extends('layouts.master')

@section('title')
    About
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/about.css') }}" />
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-6 kolom-kiri">
                <p class="title-about container">about eatducate</p>
                <p class="desc-about container">
                    Eatducate merupakan platform yang berjalan di bidang kesehatan yangmemiliki visi untuk megedukasi
                    serta membantu masyarakat terkhususnya parapengidap gizi berlebih dan obesitas untuk memberi
                    kesedaran akan hidup sehatdan tentu saja untuk memperbaiki kesehatan gizi dan menentukan pola
                    hidupyang sehat. Melalui website ini juga, pengguna tidak perlu khawatir untukmengeluarkan uang
                    dengan jumlah besar untuk sekedar berkonsultasi ataupunmengikuti program yang telah anda pilih.
                </p>
            </div>

            <div class="col-6 kolom-kanan">
                <div class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="200" height="200" viewBox="0 0 200 200"
                        fill="none">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M99.9999 200C155.229 200 200 155.228 200 99.9999C200 44.7715 155.229 0 99.9999 0C44.7715 0 0 44.7715 0 99.9999C0 155.228 44.7715 200 99.9999 200ZM115.435 77.2629C123.959 68.7384 123.959 54.9178 115.435 46.3933C106.91 37.8689 93.0899 37.8689 84.5654 46.3933C76.0414 54.9178 76.0414 68.7384 84.5654 77.2629L99.9999 92.6974L115.435 77.2629ZM122.737 115.435C131.262 123.959 145.082 123.959 153.607 115.435C162.131 106.91 162.131 93.0899 153.607 84.5654C145.082 76.0414 131.262 76.0414 122.737 84.5654L107.303 100L122.737 115.435ZM115.435 153.607C123.959 145.082 123.959 131.262 115.435 122.737L99.9999 107.303L84.5654 122.737C76.0414 131.262 76.0414 145.082 84.5654 153.607C93.0899 162.131 106.91 162.131 115.435 153.607ZM46.3932 115.435C37.869 106.91 37.869 93.0899 46.3932 84.5654C54.9179 76.0414 68.7384 76.0414 77.2629 84.5654L92.6974 100L77.2629 115.435C68.7384 123.959 54.9179 123.959 46.3932 115.435Z"
                            fill="white" />
                    </svg>
                </div>
                <p class="name">Eatducate</p>
            </div>
        </div>
    </div>
@endsection
