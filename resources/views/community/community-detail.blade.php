@extends('layouts.master')

@section('title')
    Community
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/detailCommunity.css') }}" />
@endpush

@section('content')
    <!-- Start Breadcrumb -->
    <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);"
        aria-label="breadcrumb">
        <ol class="breadcrumb container">
            <li class="breadcrumb-item"><a href="/community">Community</a></li>
            <li class="breadcrumb-item active" aria-current="page">Makanan Enak Dan Sehat</li>
        </ol>
    </nav>
    <!-- Start Breadcrumb -->

    <!-- Start Article -->
    <div class="card-article container">
        <div class="card h-100">
            <img src="{{ asset('assets/ilustration/community5.png') }}" class="card-img-top" alt="Ilustration"
                loading="lazy">
            <div class="card-body">
                <h5 class="card-title">Makanan Enak dan sehat</h5>
                <div class="d-flex align-items-center">
                    <div class="card-text d-flex align-items-center">
                        <p class="kiri-count">32k</p>
                        <p class="kiri-ket">Interaksi</p>
                    </div>
                    <div class="keterangan d-flex align-items-center">
                        <p class="kanan-count">32k</p>
                        <p class="kanan-ket">Anggota</p>
                    </div>
                    <div class="follow ms-auto">
                        <a class="btn btn-primary " href="#" role="button">follow</a>
                    </div>
                    <div class="other">
                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"
                            fill="none">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M18.3327 6.66695C18.3327 7.28581 18.0868 7.87934 17.6492 8.31694C17.2116 8.75455 16.6181 9.00039 15.9992 9.00039C15.3804 9.00039 14.7868 8.75455 14.3492 8.31694C13.9116 7.87934 13.6658 7.28581 13.6658 6.66695C13.6658 6.04808 13.9116 5.45455 14.3492 5.01695C14.7868 4.57934 15.3804 4.3335 15.9992 4.3335C16.6181 4.3335 17.2116 4.57934 17.6492 5.01695C18.0868 5.45455 18.3327 6.04808 18.3327 6.66695ZM18.3327 25.3345C18.3327 25.9534 18.0868 26.5469 17.6492 26.9845C17.2116 27.4221 16.6181 27.668 15.9992 27.668C15.3804 27.668 14.7868 27.4221 14.3492 26.9845C13.9116 26.5469 13.6658 25.9534 13.6658 25.3345C13.6658 24.7157 13.9116 24.1221 14.3492 23.6845C14.7868 23.2469 15.3804 23.0011 15.9992 23.0011C16.6181 23.0011 17.2116 23.2469 17.6492 23.6845C18.0868 24.1221 18.3327 24.7157 18.3327 25.3345ZM15.9992 18.3342C16.6181 18.3342 17.2116 18.0883 17.6492 17.6507C18.0868 17.2131 18.3327 16.6196 18.3327 16.0007C18.3327 15.3819 18.0868 14.7884 17.6492 14.3507C17.2116 13.9131 16.6181 13.6673 15.9992 13.6673C15.3804 13.6673 14.7868 13.9131 14.3492 14.3507C13.9116 14.7884 13.6658 15.3819 13.6658 16.0007C13.6658 16.6196 13.9116 17.2131 14.3492 17.6507C14.7868 18.0883 15.3804 18.3342 15.9992 18.3342Z"
                                fill="black" />
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Article -->

    {{-- Start Pertanyaan --}}
    <div class="pertanyaan container">
        <p>Punya pertanyaan seputar makanan?</p>
        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" fill="none">
            <path
                d="M21.5867 2.6665H10.4134C5.56008 2.6665 2.66675 5.55984 2.66675 10.4132V21.5732C2.66675 26.4398 5.56008 29.3332 10.4134 29.3332H21.5734C26.4267 29.3332 29.3201 26.4398 29.3201 21.5865V10.4132C29.3334 5.55984 26.4401 2.6665 21.5867 2.6665ZM24.7067 16.7065L18.9867 22.4265C18.7867 22.6265 18.5334 22.7198 18.2801 22.7198C18.0267 22.7198 17.7734 22.6265 17.5734 22.4265C17.1867 22.0398 17.1867 21.3998 17.5734 21.0132L21.5867 16.9998H8.00008C7.45341 16.9998 7.00008 16.5465 7.00008 15.9998C7.00008 15.4532 7.45341 14.9998 8.00008 14.9998H21.5867L17.5734 10.9865C17.1867 10.5998 17.1867 9.95984 17.5734 9.57317C17.9601 9.1865 18.6001 9.1865 18.9867 9.57317L24.7067 15.2932C24.8934 15.4798 25.0001 15.7332 25.0001 15.9998C25.0001 16.2665 24.8934 16.5198 24.7067 16.7065Z"
                fill="#121212" />
        </svg>
    </div>
    {{-- End Pertanyaan --}}

    {{-- Start Post --}}
    <div class="post container mb-5">
        <div class="head-admin">
            <div class="profile-head">
                <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48" fill="none">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M24 48C37.255 48 48 37.2548 48 24C48 10.7452 37.255 0 24 0C10.7452 0 0 10.7452 0 24C0 37.2548 10.7452 48 24 48ZM27.7044 18.5431C29.7501 16.4972 29.7501 13.1803 27.7044 11.1344C25.6585 9.08854 22.3416 9.08854 20.2957 11.1344C18.2499 13.1803 18.2499 16.4972 20.2957 18.5431L24 22.2474L27.7044 18.5431ZM29.457 27.7044C31.5029 29.7503 34.8198 29.7503 36.8657 27.7044C38.9114 25.6585 38.9114 22.3416 36.8657 20.2957C34.8198 18.2499 31.5029 18.2499 29.457 20.2957L25.7527 24.0001L29.457 27.7044ZM27.7044 36.8657C29.7501 34.8198 29.7501 31.5029 27.7044 29.457L24 25.7527L20.2957 29.457C18.2499 31.5029 18.2499 34.8198 20.2957 36.8657C22.3416 38.9115 25.6585 38.9115 27.7044 36.8657ZM11.1344 27.7044C9.08856 25.6585 9.08856 22.3416 11.1344 20.2957C13.1803 18.2499 16.4972 18.2499 18.5431 20.2957L22.2474 24.0001L18.5431 27.7044C16.4972 29.7503 13.1803 29.7503 11.1344 27.7044Z"
                        fill="#1C7C54" />
                </svg>
                <div class="desc-profile">
                    <p class="name">Admin</p>
                    <p class="ket">Makanan Enak dan Sehat | 4 bulan lalu</p>
                </div>
            </div>
            <div class="action">
                <div class="view d-flex">
                    <p>13k</p>
                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="22" viewBox="0 0 26 22"
                        fill="none">
                        <path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.9985 0.597168C5.79585 0.597168 -0.00415039 6.28783 -0.00415039 10.9998C-0.00415039 15.7118 5.79585 21.4025 12.9985 21.4025C20.2012 21.4025 26.0012 15.7118 26.0012 10.9998C26.0012 6.28783 20.2012 0.597168 12.9985 0.597168Z"
                            fill="#1C7C54" />
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.9982 16.2162C10.1222 16.2162 7.78223 13.8762 7.78223 11.0002C7.78223 8.12418 10.1222 5.78418 12.9982 5.78418C15.8742 5.78418 18.2142 8.12418 18.2142 11.0002C18.2142 13.8762 15.8742 16.2162 12.9982 16.2162ZM9.78222 10.9998C9.78222 9.22646 11.2249 7.78379 12.9982 7.78379C14.7716 7.78379 16.2142 9.22646 16.2142 10.9998C16.2142 12.7731 14.7716 14.2158 12.9982 14.2158C11.2249 14.2158 9.78222 12.7731 9.78222 10.9998Z"
                            fill="#1C7C54" />
                    </svg>
                </div>
                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" fill="none"
                    class="more">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M18.3327 6.66695C18.3327 7.28581 18.0868 7.87934 17.6492 8.31694C17.2116 8.75455 16.6181 9.00039 15.9992 9.00039C15.3804 9.00039 14.7868 8.75455 14.3492 8.31694C13.9116 7.87934 13.6658 7.28581 13.6658 6.66695C13.6658 6.04808 13.9116 5.45455 14.3492 5.01695C14.7868 4.57934 15.3804 4.3335 15.9992 4.3335C16.6181 4.3335 17.2116 4.57934 17.6492 5.01695C18.0868 5.45455 18.3327 6.04808 18.3327 6.66695ZM18.3327 25.3345C18.3327 25.9534 18.0868 26.5469 17.6492 26.9845C17.2116 27.4221 16.6181 27.668 15.9992 27.668C15.3804 27.668 14.7868 27.4221 14.3492 26.9845C13.9116 26.5469 13.6658 25.9534 13.6658 25.3345C13.6658 24.7157 13.9116 24.1221 14.3492 23.6845C14.7868 23.2469 15.3804 23.0011 15.9992 23.0011C16.6181 23.0011 17.2116 23.2469 17.6492 23.6845C18.0868 24.1221 18.3327 24.7157 18.3327 25.3345ZM15.9992 18.3342C16.6181 18.3342 17.2116 18.0883 17.6492 17.6507C18.0868 17.2131 18.3327 16.6196 18.3327 16.0007C18.3327 15.3819 18.0868 14.7884 17.6492 14.3507C17.2116 13.9131 16.6181 13.6673 15.9992 13.6673C15.3804 13.6673 14.7868 13.9131 14.3492 14.3507C13.9116 14.7884 13.6658 15.3819 13.6658 16.0007C13.6658 16.6196 13.9116 17.2131 14.3492 17.6507C14.7868 18.0883 15.3804 18.3342 15.9992 18.3342Z"
                        fill="#121212" />
                </svg>
            </div>
        </div>
        <div class="body">
            <p class="title-body">manfaat tomat untuk tubuh kita</p>
        </div>
        <div class="body-content">
            <p>
                Tomat adalah buah yang kaya akan nutrisi dan memiliki sejumlah manfaat untuk kesehatan tubuh. Berikut adalah
                beberapa manfaat tomat: <br><br>
            </p>

            <ol>
                <li>Sumber Antioksidan: Tomat mengandung antioksidan seperti likopen, vitamin C, dan beta-karoten yang
                    membantu
                    melindungi sel-sel tubuh dari kerusakan akibat radikal bebas. Likopen khususnya telah dikaitkan dengan
                    penurunan risiko penyakit jantung dan kanker.
                </li>
                <li>Menjaga Kesehatan Jantung: Kandungan likopen dan potassium dalam tomat dapat membantu mengatur tekanan
                    darah, sehingga dapat mendukung kesehatan jantung. Tomat juga dapat membantu menurunkan kadar kolesterol
                    LDL (kolesterol jahat).
                </li>
                <li>
                    Mendukung Kesehatan Kulit: Vitamin C dalam tomat berperan dalam produksi kolagen, yang mendukung
                    kesehatan
                    kulit. Tomat juga dapat membantu melindungi kulit dari kerusakan akibat sinar UV matahari.
                </li>
                <li>
                    Menurunkan Risiko Kanker: Beberapa studi telah menunjukkan bahwa likopen dalam tomat dapat membantu
                    mengurangi risiko beberapa jenis kanker, seperti kanker prostat, kanker payudara, dan kanker paru-paru.
                </li>
                <li>
                    Peningkatan Sistem Kekebalan Tubuh: Kandungan vitamin C dan beta-karoten dalam tomat dapat membantu
                    meningkatkan sistem kekebalan tubuh, membantu melawan infeksi dan penyakit.
                </li>
            </ol>

            <p>
                Penting untuk mencatat bahwa manfaat tomat dapat diperoleh lebih baik melalui konsumsi dalam bentuk yang
                beragam, baik mentah maupun dalam bentuk masakan seperti saus tomat atau sup. Juga, penting untuk
                memperhatikan toleransi individu dan memasukkan tomat ke dalam pola makan yang seimbang.
            </p>
        </div>

        <div class="comment">
            <div class="profile-comment">
                <p>SN</p>
            </div>
            <div class="textarea-comment">
                <div class="action-comment">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                        fill="none">
                        <path
                            d="M20.95 4.13C20.66 3.71 20.29 3.34 19.87 3.05C18.92 2.36 17.68 2 16.19 2H7.81C7.61 2 7.41 2.01 7.22 2.03C3.94 2.24 2 4.37 2 7.81V16.19C2 17.68 2.36 18.92 3.05 19.87C3.34 20.29 3.71 20.66 4.13 20.95C4.95 21.55 5.99 21.9 7.22 21.98C7.41 21.99 7.61 22 7.81 22H16.19C19.83 22 22 19.83 22 16.19V7.81C22 6.32 21.64 5.08 20.95 4.13ZM15.87 7.51H13.92L11.68 16.48H13.52C13.93 16.48 14.27 16.82 14.27 17.23C14.27 17.64 13.93 17.98 13.52 17.98H8.13C7.72 17.98 7.38 17.64 7.38 17.23C7.38 16.82 7.72 16.48 8.13 16.48H10.13L12.37 7.51H10.48C10.07 7.51 9.73 7.17 9.73 6.76C9.73 6.35 10.07 6.01 10.48 6.01H15.86C16.27 6.01 16.61 6.35 16.61 6.76C16.61 7.17 16.28 7.51 15.87 7.51Z"
                            fill="#121212" />
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                        fill="none">
                        <path
                            d="M13.75 12.75H12.25H8.5V16.74C8.5 17.02 8.73 17.25 9.01 17.25H13.75C14.99 17.25 16 16.24 16 15C16 13.76 14.99 12.75 13.75 12.75Z"
                            fill="#121212" />
                        <path
                            d="M14.5 9C14.5 7.76 13.49 6.75 12.25 6.75H9.01C8.73 6.75 8.5 6.98 8.5 7.26V11.25H12.25C13.49 11.25 14.5 10.24 14.5 9Z"
                            fill="#121212" />
                        <path
                            d="M20.95 4.13C20.66 3.71 20.29 3.34 19.87 3.05C18.92 2.36 17.68 2 16.19 2H7.81C7.61 2 7.41 2.01 7.22 2.03C3.94 2.24 2 4.37 2 7.81V16.19C2 17.68 2.36 18.92 3.05 19.87C3.34 20.29 3.71 20.66 4.13 20.95C4.95 21.55 5.99 21.9 7.22 21.98C7.41 21.99 7.61 22 7.81 22H16.19C19.83 22 22 19.83 22 16.19V7.81C22 6.32 21.64 5.08 20.95 4.13ZM13.75 18.75H9.01C7.9 18.75 7 17.85 7 16.74V12V7.26C7 6.15 7.9 5.25 9.01 5.25H12.25C14.32 5.25 16 6.93 16 9C16 9.96 15.63 10.83 15.03 11.49C16.46 12.02 17.5 13.38 17.5 15C17.5 17.07 15.82 18.75 13.75 18.75Z"
                            fill="#121212" />
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                        fill="none">
                        <path
                            d="M20.95 4.13C20.66 3.71 20.29 3.34 19.87 3.05C18.92 2.36 17.68 2 16.19 2H7.81C7.61 2 7.41 2.01 7.22 2.03C3.94 2.24 2 4.37 2 7.81V16.19C2 17.68 2.36 18.92 3.05 19.87C3.34 20.29 3.71 20.66 4.13 20.95C4.95 21.55 5.99 21.9 7.22 21.98C7.41 21.99 7.61 22 7.81 22H16.19C19.83 22 22 19.83 22 16.19V7.81C22 6.32 21.64 5.08 20.95 4.13ZM16.83 18.96H7.17C6.76 18.96 6.42 18.62 6.42 18.21C6.42 17.8 6.76 17.46 7.17 17.46H16.84C17.25 17.46 17.59 17.8 17.59 18.21C17.59 18.62 17.25 18.96 16.83 18.96ZM17.58 10.62C17.58 13.7 15.08 16.2 12 16.2C8.92 16.2 6.42 13.7 6.42 10.62V5.79C6.42 5.38 6.76 5.04 7.17 5.04C7.58 5.04 7.92 5.38 7.92 5.79V10.62C7.92 12.87 9.75 14.7 12 14.7C14.25 14.7 16.08 12.87 16.08 10.62V5.79C16.08 5.38 16.42 5.04 16.83 5.04C17.24 5.04 17.58 5.38 17.58 5.79V10.62Z"
                            fill="#121212" />
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                        fill="none">
                        <path
                            d="M12.5 14.75H10C9.59 14.75 9.25 14.41 9.25 14C9.25 13.59 9.59 13.25 10 13.25H12.5C15.12 13.25 17.25 11.12 17.25 8.5C17.25 5.88 15.12 3.75 12.5 3.75H7.5C4.88 3.75 2.75 5.88 2.75 8.5C2.75 9.6 3.14 10.67 3.84 11.52C4.1 11.84 4.06 12.31 3.74 12.58C3.42 12.84 2.95 12.8 2.68 12.48C1.76 11.36 1.25 9.95 1.25 8.5C1.25 5.05 4.05 2.25 7.5 2.25H12.5C15.95 2.25 18.75 5.05 18.75 8.5C18.75 11.95 15.95 14.75 12.5 14.75Z"
                            fill="#121212" />
                        <path
                            d="M16.5 21.75H11.5C8.05 21.75 5.25 18.95 5.25 15.5C5.25 12.05 8.05 9.25 11.5 9.25H14C14.41 9.25 14.75 9.59 14.75 10C14.75 10.41 14.41 10.75 14 10.75H11.5C8.88 10.75 6.75 12.88 6.75 15.5C6.75 18.12 8.88 20.25 11.5 20.25H16.5C19.12 20.25 21.25 18.12 21.25 15.5C21.25 14.4 20.86 13.33 20.16 12.48C19.9 12.16 19.94 11.69 20.26 11.42C20.58 11.15 21.05 11.2 21.32 11.52C22.25 12.64 22.76 14.05 22.76 15.5C22.75 18.95 19.95 21.75 16.5 21.75Z"
                            fill="#121212" />
                    </svg>
                </div>
                <div class="textArea me-auto">
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <div class="tombol-comment">
                    <a class="btn btn-success" href="#" role="button">Komentar</a>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('input', function(e) {
            if (e.target && e.target.matches('textarea.form-control')) {
                adjustTextareaHeight(e.target);
            }
        });

        function adjustTextareaHeight(textarea) {
            // Set the initial height to 60px
            textarea.style.height = '117px';

            // Calculate the scroll height of the content inside the textarea
            const scrollHeight = textarea.scrollHeight;

            // Set the height of the textarea to the calculated scroll height, with a maximum of 200px
            textarea.style.height = Math.min(scrollHeight, 117) + 'px';

            // If the scroll height is less than 200px, hide the overflow
            textarea.style.overflowY = scrollHeight > 117 ? 'auto' : 'hidden';
        }
    </script>
    {{-- End Post --}}
@endsection
