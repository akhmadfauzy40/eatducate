@extends('layouts.master')

@section('title')
    Community
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/cummunity.css') }}" />
@endpush

@section('content')
    <!-- Start Hero -->
    <div class="hero container-fluid"></div>
    <!-- End Hero -->

    <!-- Start Article -->
    <div class="card-article container mb-5">
        <div class="row row-cols-1 row-cols-md-3 g-4">
            <div class="col">
                <div class="card h-100" id="lari">
                    <img src="{{ asset('assets/ilustration/community1.png') }}" class="card-img-top" alt="Ilustration"
                        loading="lazy">
                    <div class="card-body">
                        <h5 class="card-title text-center">Lari</h5>
                        <div class="card-text d-flex justify-content-center">
                            <p class="kiri-count">32k</p>
                            <p class="kanan-count">32k</p>
                        </div>
                        <div class="keterangan d-flex justify-content-center">
                            <p class="kiri-ket">Interaksi</p>
                            <p class="kanan-ket">Anggota</p>
                        </div>
                        <div class="follow">
                            <a class="btn btn-primary" href="/community-detail" role="button">follow</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="card h-100" id="cardio">
                    <img src="{{ asset('assets/ilustration/community2.png') }}" class="card-img-top" alt="Ilustration"
                        loading="lazy">
                    <div class="card-body">
                        <h5 class="card-title text-center">cardio</h5>
                        <div class="card-text d-flex justify-content-center">
                            <p class="kiri-count">32k</p>
                            <p class="kanan-count">32k</p>
                        </div>
                        <div class="keterangan d-flex justify-content-center">
                            <p class="kiri-ket">Interaksi</p>
                            <p class="kanan-ket">Anggota</p>
                        </div>
                        <div class="follow">
                            <a class="btn btn-primary" href="/community-detail" role="button">follow</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="card h-100" id="senam">
                    <img src="{{ asset('assets/ilustration/community3.png') }}" class="card-img-top" alt="Ilustration"
                        loading="lazy">
                    <div class="card-body">
                        <h5 class="card-title text-center">Senam</h5>
                        <div class="card-text d-flex justify-content-center">
                            <p class="kiri-count">32k</p>
                            <p class="kanan-count">32k</p>
                        </div>
                        <div class="keterangan d-flex justify-content-center">
                            <p class="kiri-ket">Interaksi</p>
                            <p class="kanan-ket">Anggota</p>
                        </div>
                        <div class="follow">
                            <a class="btn btn-primary" href="/community-detail" role="button">follow</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="card h-100" id="gym">
                    <img src="{{ asset('assets/ilustration/community4.png') }}" class="card-img-top" alt="Ilustration"
                        loading="lazy">
                    <div class="card-body">
                        <h5 class="card-title text-center">gym</h5>
                        <div class="card-text d-flex justify-content-center">
                            <p class="kiri-count">32k</p>
                            <p class="kanan-count">32k</p>
                        </div>
                        <div class="keterangan d-flex justify-content-center">
                            <p class="kiri-ket">Interaksi</p>
                            <p class="kanan-ket">Anggota</p>
                        </div>
                        <div class="follow">
                            <a class="btn btn-primary" href="/community-detail" role="button">follow</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="card h-100" id="makananenakdansehat">
                    <img src="{{ asset('assets/ilustration/community5.png') }}" class="card-img-top" alt="Ilustration"
                        loading="lazy">
                    <div class="card-body">
                        <h5 class="card-title text-center">Makanan Enak dan sehat</h5>
                        <div class="card-text d-flex justify-content-center">
                            <p class="kiri-count">32k</p>
                            <p class="kanan-count">32k</p>
                        </div>
                        <div class="keterangan d-flex justify-content-center">
                            <p class="kiri-ket">Interaksi</p>
                            <p class="kanan-ket">Anggota</p>
                        </div>
                        <div class="follow">
                            <a class="btn btn-primary" href="/community-detail" role="button">follow</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="card h-100" id="diet">
                    <img src="{{ asset('assets/ilustration/community6.png') }}" class="card-img-top" alt="Ilustration"
                        loading="lazy">
                    <div class="card-body">
                        <h5 class="card-title text-center">diet</h5>
                        <div class="card-text d-flex justify-content-center">
                            <p class="kiri-count">32k</p>
                            <p class="kanan-count">32k</p>
                        </div>
                        <div class="keterangan d-flex justify-content-center">
                            <p class="kiri-ket">Interaksi</p>
                            <p class="kanan-ket">Anggota</p>
                        </div>
                        <div class="follow">
                            <a class="btn btn-primary" href="/community-detail" role="button">follow</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="card h-100" id="yoga">
                    <img src="{{ asset('assets/ilustration/community8.png') }}" class="card-img-top" alt="Ilustration"
                        loading="lazy">
                    <div class="card-body">
                        <h5 class="card-title text-center">Yoga</h5>
                        <div class="card-text d-flex justify-content-center">
                            <p class="kiri-count">32k</p>
                            <p class="kanan-count">32k</p>
                        </div>
                        <div class="keterangan d-flex justify-content-center">
                            <p class="kiri-ket">Interaksi</p>
                            <p class="kanan-ket">Anggota</p>
                        </div>
                        <div class="follow">
                            <a class="btn btn-primary" href="/community-detail" role="button">follow</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="card h-100" id="olahragadarirumah">
                    <img src="{{ asset('assets/ilustration/community7.png') }}" class="card-img-top" alt="Ilustration"
                        loading="lazy">
                    <div class="card-body">
                        <h5 class="card-title text-center">olahraga dari rumah</h5>
                        <div class="card-text d-flex justify-content-center">
                            <p class="kiri-count">32k</p>
                            <p class="kanan-count">32k</p>
                        </div>
                        <div class="keterangan d-flex justify-content-center">
                            <p class="kiri-ket">Interaksi</p>
                            <p class="kanan-ket">Anggota</p>
                        </div>
                        <div class="follow">
                            <a class="btn btn-primary" href="/community-detail" role="button">follow</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="card h-100" id="dietpuasa">
                    <img src="{{ asset('assets/ilustration/community5.png') }}" class="card-img-top" alt="Ilustration"
                        loading="lazy">
                    <div class="card-body">
                        <h5 class="card-title text-center">diet puasa</h5>
                        <div class="card-text d-flex justify-content-center">
                            <p class="kiri-count">32k</p>
                            <p class="kanan-count">32k</p>
                        </div>
                        <div class="keterangan d-flex justify-content-center">
                            <p class="kiri-ket">Interaksi</p>
                            <p class="kanan-ket">Anggota</p>
                        </div>
                        <div class="follow">
                            <a class="btn btn-primary" href="/community-detail" role="button">follow</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Article -->
@endsection

@push('scripts')
    <script>
        const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
        const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
    </script>
@endpush
