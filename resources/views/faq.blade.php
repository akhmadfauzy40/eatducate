@extends('layouts.master')

@section('title')
    FAQs
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/home.css') }}" />
@endpush

@section('content')
    <!-- Start FAQ -->
    <div class="faq container-fluid">
        <div class="title-faq container">
            <p class="text-center">frequently Asked Questions (FAQs)</p>
        </div>
        <div class="accordion1 container">
            <div class="accordion-header">
                <p>apa langkah pertama yang harus saya lakukan ketika ingin menggunakan website ini?</p>
                <div class="accordion-icon" onclick="toggleAccordion(1)">
                    <a href="#faq1" id="accordion-link-1">
                        <svg class="plus-svg" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4"
                                d="M29.3333 0H10.6667C3.85778 0 0 3.85778 0 10.6667V29.3333C0 36.1244 3.84 40 10.6667 40H29.3333C36.1422 40 40 36.1244 40 29.3333V10.6667C40 3.85778 36.1422 0 29.3333 0Z"
                                fill="#5837D0" />
                            <path
                                d="M26.6409 21.4167H21.499V26.514C21.499 27.3347 20.8279 28 20 28C19.1721 28 18.501 27.3347 18.501 26.514V21.4167H13.3591C12.5868 21.3374 12 20.6923 12 19.9226C12 19.153 12.5868 18.5079 13.3591 18.4286H18.4848V13.3473C18.5648 12.5818 19.2156 12 19.9919 12C20.7683 12 21.4191 12.5818 21.499 13.3473V18.4286H26.6409C27.4132 18.5079 28 19.153 28 19.9226C28 20.6923 27.4132 21.3374 26.6409 21.4167V21.4167Z"
                                fill="#5837D0" />
                        </svg>
                        <svg class="close-svg hidden" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                                d="M20 0.5C5.604 0.5 0.5 5.604 0.5 20C0.5 34.396 5.604 39.5 20 39.5C34.398 39.5 39.5 34.396 39.5 20C39.5 5.604 34.398 0.5 20 0.5Z"
                                fill="#D92D20" />
                            <path
                                d="M24.8007 26.286C25.1847 26.286 25.5687 26.138 25.8607 25.846C26.4467 25.26 26.4467 24.31 25.8607 23.724L22.1127 19.978L25.8387 16.25C26.4247 15.664 26.4247 14.716 25.8387 14.13C25.2527 13.544 24.3047 13.544 23.7187 14.13L19.9907 17.858L16.2587 14.126C15.6727 13.54 14.7247 13.54 14.1387 14.126C13.5527 14.712 13.5527 15.66 14.1387 16.246L17.8707 19.978L14.1387 23.714C13.5527 24.3 13.5527 25.248 14.1387 25.834C14.4307 26.128 14.8167 26.274 15.1987 26.274C15.5827 26.274 15.9667 26.128 16.2587 25.834L19.9927 22.1L23.7387 25.846C24.0327 26.138 24.4147 26.286 24.8007 26.286Z"
                                fill="#D92D20" />
                        </svg>
                    </a>
                </div>
            </div>

            <div class="line-faq container hidden" id="line-faq-1"></div>
            <div class="answer container hidden" id="answer-1">
                <p>Untuk memulainya kamu bisa menggunakan fitur BMI untuk mengetahui kategori berat badanmu,
                    memperkirakan jumlah lemak tubuh, dan menentukan apakah kamu berisiko mengalami penyakit kronis</p>
            </div>
        </div>

        <div class="accordion1 container">
            <div class="accordion-header">
                <p>fitur apa saja yang bisa dinikmati di website educate?</p>
                <div class="accordion-icon" onclick="toggleAccordion(2)">
                    <a href="#faq1" id="accordion-link-2">
                        <svg class="plus-svg" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4"
                                d="M29.3333 0H10.6667C3.85778 0 0 3.85778 0 10.6667V29.3333C0 36.1244 3.84 40 10.6667 40H29.3333C36.1422 40 40 36.1244 40 29.3333V10.6667C40 3.85778 36.1422 0 29.3333 0Z"
                                fill="#5837D0" />
                            <path
                                d="M26.6409 21.4167H21.499V26.514C21.499 27.3347 20.8279 28 20 28C19.1721 28 18.501 27.3347 18.501 26.514V21.4167H13.3591C12.5868 21.3374 12 20.6923 12 19.9226C12 19.153 12.5868 18.5079 13.3591 18.4286H18.4848V13.3473C18.5648 12.5818 19.2156 12 19.9919 12C20.7683 12 21.4191 12.5818 21.499 13.3473V18.4286H26.6409C27.4132 18.5079 28 19.153 28 19.9226C28 20.6923 27.4132 21.3374 26.6409 21.4167V21.4167Z"
                                fill="#5837D0" />
                        </svg>
                        <svg class="close-svg hidden" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                                d="M20 0.5C5.604 0.5 0.5 5.604 0.5 20C0.5 34.396 5.604 39.5 20 39.5C34.398 39.5 39.5 34.396 39.5 20C39.5 5.604 34.398 0.5 20 0.5Z"
                                fill="#D92D20" />
                            <path
                                d="M24.8007 26.286C25.1847 26.286 25.5687 26.138 25.8607 25.846C26.4467 25.26 26.4467 24.31 25.8607 23.724L22.1127 19.978L25.8387 16.25C26.4247 15.664 26.4247 14.716 25.8387 14.13C25.2527 13.544 24.3047 13.544 23.7187 14.13L19.9907 17.858L16.2587 14.126C15.6727 13.54 14.7247 13.54 14.1387 14.126C13.5527 14.712 13.5527 15.66 14.1387 16.246L17.8707 19.978L14.1387 23.714C13.5527 24.3 13.5527 25.248 14.1387 25.834C14.4307 26.128 14.8167 26.274 15.1987 26.274C15.5827 26.274 15.9667 26.128 16.2587 25.834L19.9927 22.1L23.7387 25.846C24.0327 26.138 24.4147 26.286 24.8007 26.286Z"
                                fill="#D92D20" />
                        </svg>
                    </a>
                </div>
            </div>

            <div class="line-faq container hidden" id="line-faq-2"></div>
            <div class="answer container hidden" id="answer-2">
                <p>ada banyak fitur yang bisa kamu nikmati seperti BMI calculator, konsultasi, article resep makanan,
                    dan juga komunitas</p>
            </div>
        </div>

        <div class="accordion1 container">
            <div class="accordion-header">
                <p>bagaimana cara melakukan konsultasi dengan dokter di educate</p>
                <div class="accordion-icon" onclick="toggleAccordion(3)">
                    <a href="#faq1" id="accordion-link-3">
                        <svg class="plus-svg" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4"
                                d="M29.3333 0H10.6667C3.85778 0 0 3.85778 0 10.6667V29.3333C0 36.1244 3.84 40 10.6667 40H29.3333C36.1422 40 40 36.1244 40 29.3333V10.6667C40 3.85778 36.1422 0 29.3333 0Z"
                                fill="#5837D0" />
                            <path
                                d="M26.6409 21.4167H21.499V26.514C21.499 27.3347 20.8279 28 20 28C19.1721 28 18.501 27.3347 18.501 26.514V21.4167H13.3591C12.5868 21.3374 12 20.6923 12 19.9226C12 19.153 12.5868 18.5079 13.3591 18.4286H18.4848V13.3473C18.5648 12.5818 19.2156 12 19.9919 12C20.7683 12 21.4191 12.5818 21.499 13.3473V18.4286H26.6409C27.4132 18.5079 28 19.153 28 19.9226C28 20.6923 27.4132 21.3374 26.6409 21.4167V21.4167Z"
                                fill="#5837D0" />
                        </svg>
                        <svg class="close-svg hidden" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                                d="M20 0.5C5.604 0.5 0.5 5.604 0.5 20C0.5 34.396 5.604 39.5 20 39.5C34.398 39.5 39.5 34.396 39.5 20C39.5 5.604 34.398 0.5 20 0.5Z"
                                fill="#D92D20" />
                            <path
                                d="M24.8007 26.286C25.1847 26.286 25.5687 26.138 25.8607 25.846C26.4467 25.26 26.4467 24.31 25.8607 23.724L22.1127 19.978L25.8387 16.25C26.4247 15.664 26.4247 14.716 25.8387 14.13C25.2527 13.544 24.3047 13.544 23.7187 14.13L19.9907 17.858L16.2587 14.126C15.6727 13.54 14.7247 13.54 14.1387 14.126C13.5527 14.712 13.5527 15.66 14.1387 16.246L17.8707 19.978L14.1387 23.714C13.5527 24.3 13.5527 25.248 14.1387 25.834C14.4307 26.128 14.8167 26.274 15.1987 26.274C15.5827 26.274 15.9667 26.128 16.2587 25.834L19.9927 22.1L23.7387 25.846C24.0327 26.138 24.4147 26.286 24.8007 26.286Z"
                                fill="#D92D20" />
                        </svg>
                    </a>
                </div>
            </div>

            <div class="line-faq container hidden" id="line-faq-3"></div>
            <div class="answer container hidden" id="answer-3">
                <p>kamu bisa melakukan konsultasi dengan dokter gizi yang ada di eatducate melalui menu konsultasi</p>
            </div>
        </div>

        <div class="accordion1 container">
            <div class="accordion-header">
                <p>Apakah fitur komunitas dapat membantu saya semakin giat untuk diet?</p>
                <div class="accordion-icon" onclick="toggleAccordion(4)">
                    <a href="#faq1" id="accordion-link-4">
                        <svg class="plus-svg" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4"
                                d="M29.3333 0H10.6667C3.85778 0 0 3.85778 0 10.6667V29.3333C0 36.1244 3.84 40 10.6667 40H29.3333C36.1422 40 40 36.1244 40 29.3333V10.6667C40 3.85778 36.1422 0 29.3333 0Z"
                                fill="#5837D0" />
                            <path
                                d="M26.6409 21.4167H21.499V26.514C21.499 27.3347 20.8279 28 20 28C19.1721 28 18.501 27.3347 18.501 26.514V21.4167H13.3591C12.5868 21.3374 12 20.6923 12 19.9226C12 19.153 12.5868 18.5079 13.3591 18.4286H18.4848V13.3473C18.5648 12.5818 19.2156 12 19.9919 12C20.7683 12 21.4191 12.5818 21.499 13.3473V18.4286H26.6409C27.4132 18.5079 28 19.153 28 19.9226C28 20.6923 27.4132 21.3374 26.6409 21.4167V21.4167Z"
                                fill="#5837D0" />
                        </svg>
                        <svg class="close-svg hidden" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                                d="M20 0.5C5.604 0.5 0.5 5.604 0.5 20C0.5 34.396 5.604 39.5 20 39.5C34.398 39.5 39.5 34.396 39.5 20C39.5 5.604 34.398 0.5 20 0.5Z"
                                fill="#D92D20" />
                            <path
                                d="M24.8007 26.286C25.1847 26.286 25.5687 26.138 25.8607 25.846C26.4467 25.26 26.4467 24.31 25.8607 23.724L22.1127 19.978L25.8387 16.25C26.4247 15.664 26.4247 14.716 25.8387 14.13C25.2527 13.544 24.3047 13.544 23.7187 14.13L19.9907 17.858L16.2587 14.126C15.6727 13.54 14.7247 13.54 14.1387 14.126C13.5527 14.712 13.5527 15.66 14.1387 16.246L17.8707 19.978L14.1387 23.714C13.5527 24.3 13.5527 25.248 14.1387 25.834C14.4307 26.128 14.8167 26.274 15.1987 26.274C15.5827 26.274 15.9667 26.128 16.2587 25.834L19.9927 22.1L23.7387 25.846C24.0327 26.138 24.4147 26.286 24.8007 26.286Z"
                                fill="#D92D20" />
                        </svg>
                    </a>
                </div>
            </div>

            <div class="line-faq container hidden" id="line-faq-4"></div>
            <div class="answer container hidden" id="answer-4">
                <p>tentu saja , dengan adanya fitur komunitas kamu bisa melakukan hal-hal yang susah menjadi mudah</p>
            </div>
        </div>
    </div>
    <!-- End FAQ -->
@endsection

@push('scripts')
    <script>
        function toggleAccordion(accordionNumber) {
            const lineFaq = document.getElementById('line-faq-' + accordionNumber);
            const answer = document.getElementById('answer-' + accordionNumber);
            const accordionLink = document.getElementById('accordion-link-' + accordionNumber);

            // Toggle class to show/hide elements
            lineFaq.classList.toggle('hidden');
            answer.classList.toggle('hidden');

            // Toggle class for SVG change
            const plusSvg = accordionLink.querySelector('.plus-svg');
            const closeSvg = accordionLink.querySelector('.close-svg');

            if (plusSvg && closeSvg) {
                plusSvg.classList.toggle('hidden');
                closeSvg.classList.toggle('hidden');
            }
        }
    </script>
@endpush
