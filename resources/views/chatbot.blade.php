@extends('layouts.navbar')

@section('title')
    Chatbot
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/chatbot.css') }}">
@endpush

@section('content')
    {{-- Start screenChat --}}
    <div class="screenChat container">
        <div class="logo text-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48" fill="none">
                <path
                    d="M35.82 21.4398H29.64V7.03979C29.64 3.6798 27.82 2.99979 25.6 5.51979L24 7.3398L10.46 22.7398C8.60002 24.8398 9.38002 26.5598 12.18 26.5598H18.36V40.9598C18.36 44.3198 20.18 44.9998 22.4 42.4798L24 40.6598L37.54 25.2598C39.4 23.1598 38.62 21.4398 35.82 21.4398Z"
                    fill="url(#paint0_linear_618_553)" />
                <defs>
                    <linearGradient id="paint0_linear_618_553" x1="20" y1="44" x2="32" y2="11.4998"
                        gradientUnits="userSpaceOnUse">
                        <stop stop-color="#1C7C54" />
                        <stop offset="0.0001" stop-color="#2DF8A9" />
                        <stop offset="0.525" stop-color="#4B3A8B" />
                        <stop offset="1" stop-color="#48DEA0" />
                    </linearGradient>
                </defs>
            </svg>

            <p class="greetings">Holla!! <span class="title">, selamat Datang di layanan chatbot</span></p>
            <p class="sub-greetings">Beri tahu saya apa yang Anda butuhkan, atau pilih dari saran berikut.</p>
        </div>

        {{-- Start CTA --}}
        <div class="CTA">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <path
                    d="M17.91 10.7199H14.82V3.5199C14.82 1.8399 13.91 1.4999 12.8 2.7599L12 3.6699L5.23001 11.3699C4.30001 12.4199 4.69001 13.2799 6.09001 13.2799H9.18001V20.4799C9.18001 22.1599 10.09 22.4999 11.2 21.2399L12 20.3299L18.77 12.6299C19.7 11.5799 19.31 10.7199 17.91 10.7199Z"
                    fill="url(#paint0_linear_618_558)" />
                <defs>
                    <linearGradient id="paint0_linear_618_558" x1="10" y1="22" x2="16" y2="5.74988"
                        gradientUnits="userSpaceOnUse">
                        <stop stop-color="#1C7C54" />
                        <stop offset="0.0001" stop-color="#2DF8A9" />
                        <stop offset="0.525" stop-color="#4B3A8B" />
                        <stop offset="1" stop-color="#48DEA0" />
                    </linearGradient>
                </defs>
            </svg>
            <p class="ctaText">Cobain ini dehh</p>
        </div>
        {{-- End CTA --}}

        {{-- Start Recomedation --}}
        <div class="account" style="display: none">
            <div class="profile">
                <p class="name">{{ substr(Auth::user()->name, 0, 2) }}</p>
            </div>
            <div class="chat">
                <p>olahraga yang cocok buat penyandang diabetes?</p>
            </div>
        </div>
        <div class="answer">
            <div class="logo-answer">
                <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48" fill="none">
                    <path
                        d="M35.82 21.4398H29.64V7.03979C29.64 3.6798 27.82 2.99979 25.6 5.51979L24 7.3398L10.46 22.7398C8.60002 24.8398 9.38002 26.5598 12.18 26.5598H18.36V40.9598C18.36 44.3198 20.18 44.9998 22.4 42.4798L24 40.6598L37.54 25.2598C39.4 23.1598 38.62 21.4398 35.82 21.4398Z"
                        fill="url(#paint0_linear_618_635)" />
                    <defs>
                        <linearGradient id="paint0_linear_618_635" x1="20" y1="44" x2="32"
                            y2="11.4998" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#1C7C54" />
                            <stop offset="0.0001" stop-color="#2DF8A9" />
                            <stop offset="0.525" stop-color="#4B3A8B" />
                            <stop offset="1" stop-color="#48DEA0" />
                        </linearGradient>
                    </defs>
                </svg>
            </div>
            <div class="answer-text">
                <p>
                    Olahraga merupakan salah satu pilar penting dalam pengelolaan diabetes. Olahraga yang teratur dapat
                    membantu mengontrol kadar gula darah, menurunkan berat badan, meningkatkan sensitivitas insulin, dan
                    mengurangi risiko komplikasi diabetes. <br><br>
                    Berikut adalah jenis-jenis olahraga yang cocok untuk penyandang diabetes: <br><br>
                    1. Olahraga aerobik intensitas sedang <br><br>
                    Olahraga aerobik intensitas sedang adalah olahraga yang dapat meningkatkan detak jantung dan pernapasan,
                    tetapi tidak sampai membuat Anda ngos-ngosan. Contoh olahraga aerobik intensitas sedang antara lain:
                    Jalan cepat, Bersepeda, Berenang, Senam aerobik <br><br>
                    Olahraga kekuatan <br><br>
                    Olahraga kekuatan adalah olahraga yang bertujuan untuk meningkatkan kekuatan otot. Olahraga kekuatan
                    dapat membantu meningkatkan sensitivitas insulin, menurunkan berat badan, dan mengurangi risiko patah
                    tulang. <br><br>
                    Contoh olahraga kekuatan antara lain: Angkat beban, Bodyweight training, Pilates
                </p>
            </div>
        </div>

        <div class="recomendationChat">
            <div class="chat" id="1">
                <p class="textChat">olahraga yang cocok buat penyandang diabetes?</p>
            </div>
            <div class="chat" id="2">
                <p class="textChat">Buatlah minuman enak dan sehat dari buah alpukat dan blackberry?</p>
            </div>
            <div class="chat" id="3">
                <p class="textChat">berapa kalori yang terbuang ketika tidur?</p>
            </div>
        </div>

        <script>
            document.addEventListener('DOMContentLoaded', function() {
                var chat1 = document.getElementById('1');

                chat1.addEventListener('click', function() {
                    document.querySelector('.logo').style.display = 'none';
                    document.querySelector('.CTA').style.display = 'none';
                    var recommendationChat = document.querySelector('.recomendationChat');
                    recommendationChat.style.display = 'none';

                    document.querySelector('.account').style.display = 'flex';
                    var answer = document.querySelector('.answer');
                    answer.style.display = 'flex';

                    setTimeout(function() {
                        recommendationChat.style.display = 'flex';
                    }, 500);
                });
            });
        </script>
        {{-- End Recomedation --}}
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var chatContainer = document.querySelector('.screenChat.container');

            // Tambahkan kelas auto-height jika tinggi konten melebihi 710px
            if (chatContainer.scrollHeight > 710) {
                chatContainer.classList.add('auto-height');
            }
        });
    </script>
    {{-- End screenChat --}}

    {{-- Start Textarea --}}
    <div class="input-group container mb-5">
        <textarea class="form-control" aria-label="With textarea" placeholder="Masukkan Perintahnya Di Sini" rows="1"></textarea>
        <span class="input-group-text">
            <button type="button" class="btn btn-outline-secondary">
                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" fill="none">
                    <path
                        d="M21.5867 2.6665H10.4134C5.56008 2.6665 2.66675 5.55984 2.66675 10.4132V21.5732C2.66675 26.4398 5.56008 29.3332 10.4134 29.3332H21.5734C26.4267 29.3332 29.3201 26.4398 29.3201 21.5865V10.4132C29.3334 5.55984 26.4401 2.6665 21.5867 2.6665ZM24.7067 16.7065L18.9867 22.4265C18.7867 22.6265 18.5334 22.7198 18.2801 22.7198C18.0267 22.7198 17.7734 22.6265 17.5734 22.4265C17.1867 22.0398 17.1867 21.3998 17.5734 21.0132L21.5867 16.9998H8.00008C7.45341 16.9998 7.00008 16.5465 7.00008 15.9998C7.00008 15.4532 7.45341 14.9998 8.00008 14.9998H21.5867L17.5734 10.9865C17.1867 10.5998 17.1867 9.95984 17.5734 9.57317C17.9601 9.1865 18.6001 9.1865 18.9867 9.57317L24.7067 15.2932C24.8934 15.4798 25.0001 15.7332 25.0001 15.9998C25.0001 16.2665 24.8934 16.5198 24.7067 16.7065Z"
                        fill="#121212" />
                </svg></span>
        </button>
    </div>

    <script>
        document.addEventListener('input', function(e) {
            if (e.target && e.target.matches('textarea.form-control')) {
                adjustTextareaHeight(e.target);
            }
        });

        function adjustTextareaHeight(textarea) {
            // Set the initial height to 60px
            textarea.style.height = '60px';

            // Calculate the scroll height of the content inside the textarea
            const scrollHeight = textarea.scrollHeight;

            // Set the height of the textarea to the calculated scroll height, with a maximum of 200px
            textarea.style.height = Math.min(scrollHeight, 200) + 'px';

            // If the scroll height is less than 200px, hide the overflow
            textarea.style.overflowY = scrollHeight > 200 ? 'auto' : 'hidden';
        }
    </script>

    {{-- Start Textarea --}}
@endsection
