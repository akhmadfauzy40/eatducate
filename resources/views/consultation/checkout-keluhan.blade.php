@extends('layouts.master')

@section('title')
    Checkout
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/checkoutKeluhan.css') }}" />
@endpush

@section('content')
    <!-- Start Breadcrumb -->
    <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);"
        aria-label="breadcrumb">
        <ol class="breadcrumb container">
            <li class="breadcrumb-item"><a href="/consultation">Consultation</a></li>
            <li class="breadcrumb-item"><a href="/consultation#doc1">Choose Doctor</a></li>
            <li class="breadcrumb-item"><a href="/detail-consultation">Details</a></li>
            <li class="breadcrumb-item active" aria-current="page">Checkout</li>
        </ol>
    </nav>
    <!-- End Breadcrumb -->

    {{-- Start Checkout --}}
    <div class="checkout">
        <div class="langkah">
            <div class="stepActive">
                <div class="numberActive">
                    <p class="textNumberActive">1</p>
                </div>
                <p class="textDesc">Sampaikan Keluhan</p>
            </div>
            <div class="step">
                <div class="number">
                    <p class="textNumber">2</p>
                </div>
                <p class="textDesc">Pilih Metode Konsultasi</p>
            </div>
            <div class="step">
                <div class="number">
                    <p class="textNumber">3</p>
                </div>
                <p class="textDesc">Pembayaran</p>
            </div>
        </div>
        <div class="formContainer mb-4">
            <div class="form">
                <div class="formInput">
                    <label for="tb" class="form-label">Berapa tinggi badan kamu?</label>
                    <input type="number" class="form-control" id="tb" placeholder="Masukkan TB">
                </div>
                <div class="formInput">
                    <label for="bb" class="form-label">Berapa berat badan kamu?</label>
                    <input type="number" class="form-control" id="bb" placeholder="Masukkan BB">
                </div>
                <div class="formInput">
                    <label for="bmi" class="form-label">Berapa hasil pengukuran BMI kamu?</label>
                    <input type="number" class="form-control" id="bmi" placeholder="Masukkan Hasil BMI">
                </div>
                <div class="formInput">
                    <label for="makan" class="form-label">Sehari makan berapa kali?</label>
                    <input type="number" class="form-control" id="makan" placeholder="Masukkan Hasil BMI">
                </div>
                <div class="formInput">
                    <label for="keluhan" class="form-label">Apakah ada keluhan lain? Tuliskan dibawah</label>
                    <input type="text" class="form-control" id="keluhan"
                        placeholder="Contoh: kaki mulai terasa sakit untuk berdiri dll">
                </div>
            </div>
            <div class="tombolBayar">
                <a class="btn btn-success" href="/checkout-metode-konsultasi" role="button">Submit Jawaban</a>
            </div>
        </div>
    </div>
    {{-- End Checkout --}}
@endsection
