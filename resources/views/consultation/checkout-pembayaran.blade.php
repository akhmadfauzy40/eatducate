@extends('layouts.master')

@section('title')
    Checkout
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/checkoutPembayaran.css') }}" />
@endpush

@section('content')
    <!-- Start Breadcrumb -->
    <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);"
        aria-label="breadcrumb">
        <ol class="breadcrumb container">
            <li class="breadcrumb-item"><a href="/consultation">Consultation</a></li>
            <li class="breadcrumb-item"><a href="/consultation#doc1">Choose Doctor</a></li>
            <li class="breadcrumb-item"><a href="/detail-consultation">Details</a></li>
            <li class="breadcrumb-item active" aria-current="page">Checkout</li>
        </ol>
    </nav>
    <!-- End Breadcrumb -->

    {{-- Start Checkout --}}
    <div class="checkout">
        <div class="langkah">
            <div class="stepActive">
                <div class="numberActive">
                    <p class="textNumberActive">1</p>
                </div>
                <p class="textDesc">Sampaikan Keluhan</p>
            </div>
            <div class="stepActive">
                <div class="numberActive">
                    <p class="textNumberActive">2</p>
                </div>
                <p class="textDesc">Pilih Metode Konsultasi</p>
            </div>
            <div class="stepActive">
                <div class="numberActive">
                    <p class="textNumberActive">3</p>
                </div>
                <p class="textDesc">Pembayaran</p>
            </div>
        </div>
        <div class="formContainer mb-4">
            <div class="form">
                <div class="formInput">
                    <div class="formCheck">
                        <p class="label">Credit Card</p>
                        <div class="radioButton">
                            <input type="radio" id="CreditCard" name="age" value="Credit Card">
                        </div>
                    </div>
                </div>
                <div class="formInput">
                    <div class="formCheck">
                        <p class="label">Debit Card</p>
                        <div class="radioButton">
                            <input type="radio" id="DebitCard" name="age" value="Debit Card">
                        </div>
                    </div>
                </div>
                <div class="formInput">
                    <div class="formCheck">
                        <p class="label">Transfer Bank</p>
                        <div class="radioButton">
                            <input type="radio" id="TransferBank" name="age" value="Transfer Bank">
                        </div>
                    </div>
                </div>
                <div class="formInput">
                    <div class="formCheck">
                        <p class="label">M-Banking</p>
                        <div class="radioButton">
                            <input type="radio" id="M-Banking" name="age" value="M-Banking">
                        </div>
                    </div>
                </div>
            </div>
            <div class="tombolBayar">
                <a class="btn btn-success" href="/detail-pembayaran" role="button">Bayar Sekarang</a>
            </div>
        </div>
    </div>
    {{-- End Checkout --}}
@endsection
