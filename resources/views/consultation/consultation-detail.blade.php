@extends('layouts.master')

@section('title')
    Consultation Detail
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/detailConsultation.css') }}" />
@endpush

@section('content')
    <!-- Start Breadcrumb -->
    <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);"
        aria-label="breadcrumb">
        <ol class="breadcrumb container">
            <li class="breadcrumb-item"><a href="/consultation">Consultation</a></li>
            <li class="breadcrumb-item"><a href="/consultation#doc1">Choose Doctor</a></li>
            <li class="breadcrumb-item active" aria-current="page">Details</li>
        </ol>
    </nav>
    <!-- Start Breadcrumb -->

    {{-- Start Detail-Doctor --}}
    <div class="detailDoctor">
        <div class="tentangDoc container">
            <div class="profileDoc">
                <img src="{{ asset('assets/profile/dokter.jpeg') }}" alt="Profile" class="profileImg">
                <div class="profileContainer">
                    <div class="profileKet">
                        <p class="profileName">dr Claudia Putri, Msi, Sp.GK</p>
                        <div class="type">
                            <div class="profesi d-flex align-items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"
                                    fill="none">
                                    <path
                                        d="M17.9667 8.95046L16.8334 7.63379C16.6167 7.38379 16.4417 6.91712 16.4417 6.58379V5.16712C16.4417 4.28379 15.7167 3.55879 14.8334 3.55879H13.4167C13.0917 3.55879 12.6167 3.38379 12.3667 3.16712L11.0501 2.03379C10.4751 1.54212 9.5334 1.54212 8.95007 2.03379L7.64173 3.17546C7.39173 3.38379 6.91673 3.55879 6.59173 3.55879H5.15007C4.26673 3.55879 3.54173 4.28379 3.54173 5.16712V6.59212C3.54173 6.91712 3.36673 7.38379 3.1584 7.63379L2.0334 8.95879C1.55007 9.53379 1.55007 10.4671 2.0334 11.0421L3.1584 12.3671C3.36673 12.6171 3.54173 13.0838 3.54173 13.4088V14.8338C3.54173 15.7171 4.26673 16.4421 5.15007 16.4421H6.59173C6.91673 16.4421 7.39173 16.6171 7.64173 16.8338L8.9584 17.9671C9.5334 18.4588 10.4751 18.4588 11.0584 17.9671L12.3751 16.8338C12.6251 16.6171 13.0917 16.4421 13.4251 16.4421H14.8417C15.7251 16.4421 16.4501 15.7171 16.4501 14.8338V13.4171C16.4501 13.0921 16.6251 12.6171 16.8417 12.3671L17.9751 11.0505C18.4584 10.4755 18.4584 9.52546 17.9667 8.95046ZM13.4667 8.42546L9.44173 12.4505C9.32507 12.5671 9.16673 12.6338 9.00007 12.6338C8.8334 12.6338 8.67507 12.5671 8.5584 12.4505L6.54173 10.4338C6.30007 10.1921 6.30007 9.79212 6.54173 9.55046C6.7834 9.30879 7.1834 9.30879 7.42507 9.55046L9.00007 11.1255L12.5834 7.54212C12.8251 7.30046 13.2251 7.30046 13.4667 7.54212C13.7084 7.78379 13.7084 8.18379 13.4667 8.42546Z"
                                        fill="#1C7C54" />
                                </svg>
                                <p class="desc">Dokter Gizi</p>
                            </div>
                            <div class="rs d-flex align-items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"
                                    fill="none">
                                    <path
                                        d="M18.3337 17.708H1.66699C1.32533 17.708 1.04199 17.9913 1.04199 18.333C1.04199 18.6747 1.32533 18.958 1.66699 18.958H18.3337C18.6753 18.958 18.9587 18.6747 18.9587 18.333C18.9587 17.9913 18.6753 17.708 18.3337 17.708Z"
                                        fill="#F04438" />
                                    <path
                                        d="M14.1667 1.66699H5.83333C3.33333 1.66699 2.5 3.15866 2.5 5.00033V18.3337H7.5V13.2837C7.5 12.8503 7.85 12.5003 8.28333 12.5003H11.725C12.15 12.5003 12.5083 12.8503 12.5083 13.2837V18.3337H17.5083V5.00033C17.5 3.15866 16.6667 1.66699 14.1667 1.66699ZM12.0833 7.70866H10.625V9.16699C10.625 9.50866 10.3417 9.79199 10 9.79199C9.65833 9.79199 9.375 9.50866 9.375 9.16699V7.70866H7.91667C7.575 7.70866 7.29167 7.42533 7.29167 7.08366C7.29167 6.74199 7.575 6.45866 7.91667 6.45866H9.375V5.00033C9.375 4.65866 9.65833 4.37533 10 4.37533C10.3417 4.37533 10.625 4.65866 10.625 5.00033V6.45866H12.0833C12.425 6.45866 12.7083 6.74199 12.7083 7.08366C12.7083 7.42533 12.425 7.70866 12.0833 7.70866Z"
                                        fill="#F04438" />
                                </svg>
                                <p class="desc">RSUD Prof. Dr. Margono Soekarjo</p>
                            </div>
                        </div>
                    </div>
                    <div class="biaya">
                        <div class="consulCost">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"
                                fill="none">
                                <path
                                    d="M10.625 13.2666H11.1667C11.7083 13.2666 12.1583 12.7833 12.1583 12.1999C12.1583 11.4749 11.9 11.3333 11.475 11.1833L10.6333 10.8916V13.2666H10.625Z"
                                    fill="#98A2B3" />
                                <path
                                    d="M9.97538 1.58302C5.37538 1.59969 1.65038 5.34136 1.66705 9.94136C1.68371 14.5414 5.42538 18.2664 10.0254 18.2497C14.6254 18.233 18.3504 14.4914 18.3337 9.89135C18.317 5.29135 14.5754 1.57469 9.97538 1.58302ZM11.8837 9.99969C12.5337 10.2247 13.4087 10.708 13.4087 12.1997C13.4087 13.483 12.4004 14.5164 11.167 14.5164H10.6254V14.9997C10.6254 15.3414 10.342 15.6247 10.0004 15.6247C9.65871 15.6247 9.37538 15.3414 9.37538 14.9997V14.5164H9.07538C7.70872 14.5164 6.60038 13.3664 6.60038 11.9497C6.60038 11.608 6.88371 11.3247 7.22538 11.3247C7.56705 11.3247 7.85038 11.608 7.85038 11.9497C7.85038 12.6747 8.40038 13.2664 9.07538 13.2664H9.37538V10.4497L8.11705 9.99969C7.46705 9.77469 6.59205 9.29135 6.59205 7.79969C6.59205 6.51635 7.60038 5.48302 8.83372 5.48302H9.37538V4.99969C9.37538 4.65802 9.65871 4.37469 10.0004 4.37469C10.342 4.37469 10.6254 4.65802 10.6254 4.99969V5.48302H10.9254C12.292 5.48302 13.4004 6.63302 13.4004 8.04969C13.4004 8.39135 13.117 8.67469 12.7754 8.67469C12.4337 8.67469 12.1504 8.39135 12.1504 8.04969C12.1504 7.32469 11.6004 6.73302 10.9254 6.73302H10.6254V9.54969L11.8837 9.99969Z"
                                    fill="#98A2B3" />
                                <path
                                    d="M7.84961 7.8084C7.84961 8.5334 8.10794 8.67507 8.53294 8.82507L9.37461 9.11673V6.7334H8.83294C8.29128 6.7334 7.84961 7.21673 7.84961 7.8084Z"
                                    fill="#98A2B3" />
                            </svg>
                            <p class="costDesc">Biaya konsultasi Rp 100.000</p>
                        </div>
                        <div class="consulCost">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"
                                fill="none">
                                <path
                                    d="M10.0003 1.66699C5.40866 1.66699 1.66699 5.40866 1.66699 10.0003C1.66699 14.592 5.40866 18.3337 10.0003 18.3337C14.592 18.3337 18.3337 14.592 18.3337 10.0003C18.3337 5.40866 14.592 1.66699 10.0003 1.66699ZM9.37533 6.66699C9.37533 6.32533 9.65866 6.04199 10.0003 6.04199C10.342 6.04199 10.6253 6.32533 10.6253 6.66699V10.8337C10.6253 11.1753 10.342 11.4587 10.0003 11.4587C9.65866 11.4587 9.37533 11.1753 9.37533 10.8337V6.66699ZM10.767 13.6503C10.7253 13.7587 10.667 13.842 10.592 13.9253C10.5087 14.0003 10.417 14.0587 10.317 14.1003C10.217 14.142 10.1087 14.167 10.0003 14.167C9.89199 14.167 9.78366 14.142 9.68366 14.1003C9.58366 14.0587 9.49199 14.0003 9.40866 13.9253C9.33366 13.842 9.27533 13.7587 9.23366 13.6503C9.19199 13.5503 9.16699 13.442 9.16699 13.3337C9.16699 13.2253 9.19199 13.117 9.23366 13.017C9.27533 12.917 9.33366 12.8253 9.40866 12.742C9.49199 12.667 9.58366 12.6087 9.68366 12.567C9.88366 12.4837 10.117 12.4837 10.317 12.567C10.417 12.6087 10.5087 12.667 10.592 12.742C10.667 12.8253 10.7253 12.917 10.767 13.017C10.8087 13.117 10.8337 13.2253 10.8337 13.3337C10.8337 13.442 10.8087 13.5503 10.767 13.6503Z"
                                    fill="#98A2B3" />
                            </svg>
                            <p class="costDesc">16 orang sudah melakukan konsultasi</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="descDoc">
                <div class="descDoc-title">
                    <p>Tentang Dokter</p>
                </div>
                <div class="descDoc-desc">
                    <p>dr Claudia Putri, Msi, Sp.Gk merupakan seorang dokter Psikologi atau akrab disebut Psikolog. Dirinya
                        saat ini aktif melayani pasien di RSUD Prof. dR. Margono Soekarjo. Untuk sisi akademisnya, beliau
                        telah menamatkan pendidikan medis di Universitas Indonesia. Berbekal pengalamannya selama ini,
                        dirinya dapat memberikan layanan kesehatan yang lengkap secara ramah dan edukatif kepada pasien.
                        Disertai tindakan medis yang diperlukan. </p>
                </div>
            </div>
        </div>
        <div class="cv container">
            <div class="pendidikan">
                <p class="pendidikanTitle">Pendidikan</p>
                <div class="univ">
                    <p class="universitas">Universitas Indonesia</p>
                    <p class="prodi">Sarjana Ilmu Gizi Klinik</p>
                </div>
                <div class="univ">
                    <p class="universitas">Universitas Indonesia</p>
                    <p class="prodi">Magister Gizi</p>
                </div>
            </div>
            <div class="pengalaman">
                <p class="pengalamanTitle">Pengalaman</p>
                <div class="univ">
                    <p class="universitas">Psikolog</p>
                    <p class="prodi">RSUP Dr Kariadi</p>
                </div>
                <div class="univ">
                    <p class="universitas">Psikolog</p>
                    <p class="prodi">RSUD Prof. Dr. Margono Soekarjo</p>
                </div>
            </div>
            <div class="keanggotaan">
                <p class="KeanggotaanTitle">Keanggotaan</p>
                <p class="keanggotaanDesc">Ikatan Dokter Indonesia (IDI)</p>
                <p class="keanggotaanDesc">Himpunan Psikologi Indonesia (HIMPSI)</p>
            </div>
        </div>
    </div>
    {{-- End Detail-Doctor --}}

    {{-- Start Pesan --}}
    <div class="pesanan container d-flex mb-5">
        <div class="jadwal">
            <div class="aturJadwal">
                <p class="aturJadwal-title text-center">Atur Jadwal Konsultasi</p>
                <div class="pilihTanggal">
                    <label for="tanggal" class="form-label">Pilih Tanggal</label>
                    <input type="date" class="form-control" id="tanggal" placeholder="Pilih Tanggal Konsultasi"
                        required>

                </div>
                <div class="pilihSesi">
                    <p class="label">Pilih Sesi</p>
                    <div class="jamSesi">
                        <input type="radio" class="btn-check" name="options" id="option1" autocomplete="off">
                        <label class="btn btn-primary" for="option1">10:30 - 11:00</label>

                        <input type="radio" class="btn-check" name="options" id="option2" autocomplete="off">
                        <label class="btn btn-primary" for="option2">11:00 - 11:30</label>

                        <input type="radio" class="btn-check" name="options" id="option3" autocomplete="off">
                        <label class="btn btn-primary" for="option3">11:30 - 12:00</label>

                        <input type="radio" class="btn-check" name="options" id="option4" autocomplete="off">
                        <label class="btn btn-primary" for="option4">12:30 - 13:00</label>

                        <input type="radio" class="btn-check" name="options" id="option5" autocomplete="off">
                        <label class="btn btn-primary" for="option5">13:00 - 13:30</label>

                        <input type="radio" class="btn-check" name="options" id="option6" autocomplete="off">
                        <label class="btn btn-primary" for="option6">13:30 - 14:00</label>
                    </div>
                </div>
            </div>
            <div class="tombolPesan">
                <a class="btn btn-success" href="/checkout-keluhan" role="button">lanjutkan pesanan</a>
            </div>
        </div>
        <div class="jamReg">
            <div class="jamReg-title">
                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"
                    fill="none">
                    <path opacity="0.4"
                        d="M15.9998 29.3337C23.3636 29.3337 29.3332 23.3641 29.3332 16.0003C29.3332 8.63653 23.3636 2.66699 15.9998 2.66699C8.63604 2.66699 2.6665 8.63653 2.6665 16.0003C2.6665 23.3641 8.63604 29.3337 15.9998 29.3337Z"
                        fill="#175CD3" />
                    <path
                        d="M20.9467 21.2403C20.7734 21.2403 20.6 21.2003 20.44 21.0937L16.3067 18.627C15.28 18.0137 14.52 16.667 14.52 15.4803V10.0137C14.52 9.46701 14.9734 9.01367 15.52 9.01367C16.0667 9.01367 16.52 9.46701 16.52 10.0137V15.4803C16.52 15.9603 16.92 16.667 17.3334 16.907L21.4667 19.3737C21.9467 19.6537 22.0934 20.267 21.8134 20.747C21.6134 21.067 21.28 21.2403 20.9467 21.2403Z"
                        fill="#175CD3" />
                </svg>
                <p class="text-title">Jam Reguler</p>
            </div>
            <div class="hariTanggalCon">
                <div class="hariTanggal">
                    <p class="hari">Senin</p>
                    <p class="jam">10:00 - 14:00</p>
                </div>
                <div class="hariTanggal">
                    <p class="hari">Selasa</p>
                    <p class="jam">10:00 - 14:00</p>
                </div>
                <div class="hariTanggal">
                    <p class="hari">Rabu</p>
                    <p class="jam">10:00 - 14:00</p>
                </div>
                <div class="hariTanggal">
                    <p class="hari">Kamis</p>
                    <p class="jam">10:00 - 14:00</p>
                </div>
                <div class="hariTanggal">
                    <p class="hari">Jumat</p>
                    <p class="jam">10:00 - 14:00</p>
                </div>
                <div class="hariTanggal">
                    <p class="hari">Sabtu</p>
                    <p class="jam">10:00 - 14:00</p>
                </div>
            </div>
        </div>
    </div>
    {{-- End Pesan --}}
@endsection

@push('scripts')
    <script>
        const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
        const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
    </script>
@endpush
