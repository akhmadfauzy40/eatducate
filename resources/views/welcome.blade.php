@extends('layouts.master')

@section('title')
    Home
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/home.css') }}" />
@endpush

@section('content')
    <!-- Start Hero -->
    <div class="hero container-fluid">
        <div class="elips1"></div>
        <div class="elips2"></div>
        <div class="ornamen">
            <svg xmlns="http://www.w3.org/2000/svg" width="122" height="162" viewBox="0 0 162 162" fill="none">
                <g clip-path="url(#clip0_141_546)">
                    <mask id="mask0_141_546" style="mask-type:luminance" maskUnits="userSpaceOnUse" x="0" y="0"
                        width="162" height="162">
                        <path d="M162 0H0V162H162V0Z" fill="white" />
                    </mask>
                    <g mask="url(#mask0_141_546)">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M86.7858 0H75.2142V51.235L56.0213 3.73071L45.2924 8.06545L64.9827 56.8004L27.8155 19.6332L19.6333 27.8155L55.2563 63.4385L9.09152 43.2687L4.45872 53.8722L53.3065 75.2142H0V86.7858H53.3064L4.45873 108.128L9.09152 118.731L55.2563 98.5616L19.6333 134.185L27.8155 142.366L64.9827 105.2L45.2924 153.935L56.0213 158.269L75.2142 110.765V162H86.7858V110.765L105.979 158.269L116.707 153.935L97.0178 105.2L134.185 142.366L142.366 134.185L106.743 98.5616L152.909 118.731L157.541 108.128L108.694 86.7858H162V75.2142H108.693L157.541 53.8722L152.909 43.2687L106.743 63.4385L142.366 27.8154L134.185 19.6332L97.0178 56.8003L116.707 8.06545L105.979 3.73071L86.7858 51.235V0Z"
                            fill="url(#paint0_linear_141_546)" />
                    </g>
                </g>
                <defs>
                    <linearGradient id="paint0_linear_141_546" x1="11.34" y1="21.06" x2="144.99" y2="145.395"
                        gradientUnits="userSpaceOnUse">
                        <stop stop-color="#1B512D" />
                        <stop offset="1" stop-color="#B1CF5F" />
                    </linearGradient>
                    <clipPath id="clip0_141_546">
                        <rect width="162" height="162" fill="white" />
                    </clipPath>
                </defs>
            </svg>
        </div>
        <div class="headline container">
            <h1 class="text-center align-items-center">Your best choice for healthy lifestyle</h1>
            <p>membantu anda lebih fokus untuk mengejar berat badan ideal dengan gaya hidup sehat</p>
            <div class="button">
                <button type="button" class="btn btn-success">Get Started</button>
            </div>
        </div>
    </div>
    <!-- End Hero -->

    <!-- Start BMI -->
    <form action="/calculate-bmi" method="post">
        @csrf
        <div class="bmi container-fluid" id="bmi">
            <h1 class="title-bmi container">
                BMI Calculator
            </h1>
            <p class="description">
                Hitung dan pantau perjalanan kebugaranmu! Nikmati pengalaman yang menyenangkan sambil menjaga kesehatan dan
                mencapai tujuanmu. 💪🌟
            </p>

            <div class="form">
                <div class="row">
                    <div class="col">
                        <label for="inputEmail4" class="form-label">Height</label>
                        <div class="input-group">
                            <div class="input-group-text">Cm</div>
                            <input type="number" class="form-control" name="height" id="autoSizingInputGroup">
                        </div>
                    </div>
                    <div class="col">
                        <label for="inputEmail4" class="form-label">Weight</label>
                        <div class="input-group">
                            <div class="input-group-text">Kg</div>
                            <input type="number" class="form-control" name="weight" id="autoSizingInputGroup">
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-success calculate" type="submit">Calculate</button>
        </div>
    </form>
    <!-- End BMI -->

    <!-- Start Fitur -->
    <div class="fitur container-fluid">
        <h1 class="title-fitur">
            why you should trust us get know about us
        </h1>
        <div class="row align-items-start text center">
            <div class="col col-icon">
                <div class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"
                        fill="none">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M20.9606 13.6288L15.7633 18.8274C15.5753 19.0154 15.3219 19.1208 15.0566 19.1208C14.7913 19.1208 14.5366 19.0154 14.3486 18.8274L11.8259 16.3021C11.4353 15.9114 11.4353 15.2768 11.8273 14.8874C12.2166 14.4968 12.8499 14.4968 13.2406 14.8874L15.0566 16.7061L19.5459 12.2141C19.9366 11.8234 20.5699 11.8234 20.9606 12.2141C21.3513 12.6048 21.3513 13.2381 20.9606 13.6288ZM26.2499 6.43409C25.4419 5.62476 21.0046 2.76343 15.9993 2.76343C10.9913 2.76343 6.55659 5.62476 5.74859 6.43409C4.97392 7.21009 4.98592 7.81409 5.04459 11.1661C5.06992 12.5261 5.10326 14.3768 5.10326 16.9341C5.10326 25.6274 12.1993 29.2368 15.9993 29.2368C19.7979 29.2368 26.8953 25.6274 26.8953 16.9341C26.8953 14.3754 26.9286 12.5234 26.9539 11.1648C27.0139 7.81276 27.0246 7.21009 26.2499 6.43409Z"
                            fill="white" />
                    </svg>
                </div>
                <div class="title-icon">
                    <h3>Nutritionist</h3>
                </div>
                <div class="description-icon">
                    <p>konsultasikan kesehatanmu ke ahli gizi yang kamu punya</p>
                </div>
            </div>
            <div class="col col-icon">
                <div class="col col-icon">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"
                            fill="none">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M21.2672 17.8879C20.5312 17.8879 19.9272 17.2919 19.9272 16.5546C19.9272 15.8172 20.5179 15.2212 21.2552 15.2212H21.2672C22.0032 15.2212 22.6005 15.8172 22.6005 16.5546C22.6005 17.2919 22.0032 17.8879 21.2672 17.8879ZM15.9219 17.8879C15.1845 17.8879 14.5819 17.2919 14.5819 16.5546C14.5819 15.8172 15.1739 15.2212 15.9099 15.2212H15.9219C16.6579 15.2212 17.2552 15.8172 17.2552 16.5546C17.2552 17.2919 16.6579 17.8879 15.9219 17.8879ZM10.5765 17.8879C9.84052 17.8879 9.23785 17.2919 9.23785 16.5546C9.23785 15.8172 9.82719 15.2212 10.5645 15.2212H10.5765C11.3125 15.2212 11.9099 15.8172 11.9099 16.5546C11.9099 17.2919 11.3125 17.8879 10.5765 17.8879ZM25.9019 6.10124C23.2605 3.45991 19.7445 2.00391 16.0032 2.00391C12.2619 2.00391 8.74585 3.45991 6.10452 6.10124C1.96852 10.2386 0.840522 16.5879 3.27386 21.8332C3.31786 22.0492 3.19919 22.9279 3.11386 23.5706C2.79119 25.9786 2.62586 27.7466 3.44319 28.5652C4.25919 29.3826 6.02585 29.2172 8.43385 28.8932C9.07652 28.8079 9.96586 28.6932 10.1045 28.7079C11.9792 29.5759 13.9832 29.9959 15.9725 29.9959C19.6232 29.9959 23.2259 28.5799 25.9019 25.9026C31.3592 20.4426 31.3605 11.5599 25.9019 6.10124Z"
                                fill="white" />
                        </svg>
                    </div>
                    <div class="title-icon">
                        <h3>Chatbot Support</h3>
                    </div>
                    <div class="description-icon">
                        <p>konsultasikan kesehatanmu ke ahli gizi yang kamu punya</p>
                    </div>
                </div>
            </div>
            <div class="col col-icon">
                <div class="col col-icon">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="28" viewBox="0 0 34 28"
                            fill="none">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M25.593 12.4913H25.6197C28.4597 12.4913 30.7797 10.1713 30.7797 7.34465C30.7797 4.50465 28.4597 2.18465 25.6197 2.18465C25.0597 2.18465 24.5264 2.27799 24.0197 2.43799C24.9397 3.79799 25.473 5.43799 25.473 7.21132C25.4864 9.01132 24.9264 10.7447 23.873 12.1847C24.4197 12.3847 24.993 12.4913 25.593 12.4913Z"
                                fill="white" />
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M8.36639 12.4913H8.40639C9.00639 12.4913 9.57973 12.3847 10.1264 12.1847C9.11306 10.798 8.51306 9.07799 8.51306 7.21132C8.51306 5.43799 9.04639 3.79799 9.96639 2.43799C9.45973 2.27799 8.92639 2.18465 8.36639 2.18465C5.52639 2.18465 3.21973 4.50465 3.21973 7.34465C3.21973 10.1713 5.52639 12.4913 8.36639 12.4913Z"
                                fill="white" />
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M16.9938 13.6994H17.0378C18.7645 13.6941 20.3858 13.0154 21.6018 11.7901C22.8192 10.5648 23.4858 8.93943 23.4792 7.21543C23.4792 3.63809 20.5712 0.72876 16.9938 0.72876C13.4178 0.72876 10.5098 3.63809 10.5098 7.21543C10.5098 10.7901 13.4178 13.6994 16.9938 13.6994Z"
                                fill="white" />
                            <g opacity="0.4">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M25.6329 14.4111C24.1129 14.4111 22.6862 14.7178 21.4862 15.2378C25.6862 16.4511 28.6596 19.3578 28.7396 22.7578C30.9262 22.4778 33.2862 21.6511 33.2862 19.3978C33.2862 16.6911 29.7796 14.4111 25.6329 14.4111Z"
                                    fill="white" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M12.4996 15.2245C11.2996 14.7178 9.87289 14.4111 8.36622 14.4111C4.21956 14.4111 0.712891 16.6911 0.712891 19.3978C0.712891 21.6511 3.05956 22.4645 5.24622 22.7578C5.31289 19.3578 8.28622 16.4511 12.4996 15.2245Z"
                                    fill="white" />
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M16.9924 16.5938C11.711 16.5938 7.24837 19.4685 7.24837 22.8712C7.24837 27.2712 14.5817 27.2712 16.9924 27.2712C21.1764 27.2712 26.739 26.8178 26.739 22.8992C26.739 19.4805 22.275 16.5938 16.9924 16.5938Z"
                                    fill="white" />
                            </g>
                        </svg>
                    </div>
                    <div class="title-icon">
                        <h3>Community</h3>
                    </div>
                    <div class="description-icon">
                        <p>Temukan teman seiring perjalanan diet Anda</p>
                    </div>
                </div>
            </div>
            <div class="col col-icon">
                <div class="col col-icon">
                    <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"
                            fill="none">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M28.575 12.9727C28.059 10.778 25.7684 10.6647 23.927 10.5727C22.7684 10.514 21.5697 10.4553 21.0684 9.93665C20.5577 9.40865 20.159 8.39665 19.771 7.41798C19.015 5.50332 18.1577 3.33398 15.999 3.33398C13.8417 3.33398 12.9857 5.50332 12.2284 7.41798C11.8417 8.39665 11.4417 9.40865 10.9324 9.93665C10.4297 10.4553 9.23105 10.514 8.07238 10.5727C6.23105 10.6647 3.94038 10.778 3.42438 12.9727C2.93505 15.0553 4.46438 16.3153 5.81371 17.4273C6.73638 18.1873 7.60838 18.9073 7.80171 19.7873C7.98971 20.642 7.70705 21.746 7.43371 22.8153C6.96438 24.654 6.43105 26.7367 8.21105 28.0793C8.76705 28.4993 9.33105 28.666 9.88838 28.666C11.095 28.666 12.271 27.8873 13.2777 27.2207C14.2097 26.6047 15.1737 25.966 15.999 25.966C16.8244 25.966 17.7897 26.6047 18.7204 27.2207C20.1937 28.1953 22.0257 29.406 23.7897 28.0793C25.5697 26.7367 25.0364 24.6527 24.5657 22.8127C24.2924 21.7447 24.011 20.6407 24.1977 19.7873C24.391 18.9073 25.263 18.1887 26.1857 17.4273C27.535 16.3153 29.0644 15.0553 28.575 12.9727Z"
                                fill="white" />
                        </svg>
                    </div>
                    <div class="title-icon">
                        <h3>Tips & Trick</h3>
                    </div>
                    <div class="description-icon">
                        <p>temukan tips dan trik diet menyenangkan</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Fitur -->

    <!-- Start Menu -->
    <div class="menu container-fluid">
        <div class="elips3"></div>
        <div class="elips4"></div>
        <div class="elips5"></div>
        <div class="menu1 container">
            <div class="row menu-1">
                <div class="col-md-6">
                    <p class="title-menu1">
                        24/7 Virtual Assistant <br>
                    </p>
                    <p class="description-menu1">
                        Dapatkan bantuan instan dan jawaban atas pertanyaan seputar diet, nutrisi, dan kesehatan melalui
                        asisten virtual cerdas kami yang siap membantu Anda kapan pun dibutuhkan
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="ilustration">
                        <img src="{{ asset('assets/ilustration/ilustration1.png') }}" alt="Ilustration" height="auto"
                            width="380" loading="lazy">
                    </div>
                </div>
            </div>
        </div>

        <div class="menu2 container">
            <div class="row menu-2">
                <div class="col-md-4">
                    <div class="ilustration">
                        <img src="{{ asset('assets/ilustration/ilustration2.png') }}" alt="Ilustration" height="auto"
                            width="380" loading="lazy">
                    </div>
                </div>
                <div class="col-md-6">
                    <p class="title-menu2">
                        Consult with our nutritionist <br>
                    </p>
                    <p class="description-menu2">
                        Dapatkan panduan khusus dari ahli gizi berpengalaman untuk membantu Anda merancang rencana diet
                        yang sesuai dengan kebutuhan tubuh Anda
                    </p>
                </div>
            </div>
        </div>

        <div class="menu3 container">
            <div class="row menu-3">
                <div class="col-md-6">
                    <p class="title-menu3">
                        let's grow together with the community <br>
                    </p>
                    <p class="description-menu1">
                        Temukan teman seiring perjalanan diet Anda, berbagi pengalaman, serta memberikan dan mendapatkan
                        dukungan positif dalam komunitas yang peduli tentang kesehatan
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="ilustration">
                        <img src="{{ asset('assets/ilustration/ilustration3.png') }}" alt="Ilustration" height="auto"
                            width="380" loading="lazy">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Menu -->

    <!-- Start Testimoni -->
    <div class="testimoni container-fluid">
        <div class="container">
            <p class="title-testimoni text-center">What our users say about us</p>

            <div class="row row-cols-1 row-cols-md-3 g-4">
                <div class="col">
                    <div class="card h-100">
                        <div class="card-body">
                            <img src="{{ asset('assets/profile/profile1.png') }}" alt="Profile" class="profile-img"
                                loading="lazy">
                            <div class="d-flex flex-column ms-2">
                                <p class="card-title">Melati</p>
                                <p class="card-subtitle">Mahasiswa ITTP</p>

                            </div>
                            <p class="card-text">
                                “saya sangat terkesan dengan komitmen website ini untuk menyediakan informasi seputar
                                makanan sehat dengan pendekatan yang enak dan menyenangkan. dan fitur komunitasnya
                                memberikan ruang yang luar biasa untuk berbagi inspirasi dengan sesama pencari
                                kesehatan.”
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="card h-100">
                        <div class="card-body testimoni-body">
                            <img src="{{ asset('assets/profile/profile2.png') }}" alt="Profile" class="profile-img"
                                loading="lazy">
                            <div class="d-flex flex-column ms-2">
                                <p class="card-title">michael</p>
                                <p class="card-subtitle">Mahasiswa ITTP</p>

                            </div>
                            <p class="card-text">
                                “Fitur komunitasnya memberikan pengalaman yang menyenangkan, sementara konsultasi dengan
                                ahli gizi memberikan panduan yang sangat berharga. Saya sangat merekomendasikan bagi
                                siapa pun yang ingin menciptakan perubahan positif dalam gaya hidup mereka”
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="card h-100">
                        <div class="card-body testimoni-body">
                            <img src="{{ asset('assets/profile/profile3.png') }}" alt="Profile" class="profile-img"
                                loading="lazy">
                            <div class="d-flex flex-column ms-2">
                                <p class="card-title">Kartika</p>
                                <p class="card-subtitle">Mahasiswa IT Telkom Purwokerto</p>

                            </div>
                            <p class="card-text">
                                “Terima kasih atas upaya luar biasa dalam membantu saya mencapai tujuan kesehatan saya!”
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Testimoni -->

    <!-- Start FAQ -->
    <div class="faq container-fluid">
        <div class="title-faq container">
            <p class="text-center">frequently Asked Questions (FAQs)</p>
        </div>
        <div class="accordion1 container">
            <div class="accordion-header">
                <p>apa langkah pertama yang harus saya lakukan ketika ingin menggunakan website ini?</p>
                <div class="accordion-icon" onclick="toggleAccordion(1)">
                    <a href="#faq1" id="accordion-link-1">
                        <svg class="plus-svg" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4"
                                d="M29.3333 0H10.6667C3.85778 0 0 3.85778 0 10.6667V29.3333C0 36.1244 3.84 40 10.6667 40H29.3333C36.1422 40 40 36.1244 40 29.3333V10.6667C40 3.85778 36.1422 0 29.3333 0Z"
                                fill="#5837D0" />
                            <path
                                d="M26.6409 21.4167H21.499V26.514C21.499 27.3347 20.8279 28 20 28C19.1721 28 18.501 27.3347 18.501 26.514V21.4167H13.3591C12.5868 21.3374 12 20.6923 12 19.9226C12 19.153 12.5868 18.5079 13.3591 18.4286H18.4848V13.3473C18.5648 12.5818 19.2156 12 19.9919 12C20.7683 12 21.4191 12.5818 21.499 13.3473V18.4286H26.6409C27.4132 18.5079 28 19.153 28 19.9226C28 20.6923 27.4132 21.3374 26.6409 21.4167V21.4167Z"
                                fill="#5837D0" />
                        </svg>
                        <svg class="close-svg hidden" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                                d="M20 0.5C5.604 0.5 0.5 5.604 0.5 20C0.5 34.396 5.604 39.5 20 39.5C34.398 39.5 39.5 34.396 39.5 20C39.5 5.604 34.398 0.5 20 0.5Z"
                                fill="#D92D20" />
                            <path
                                d="M24.8007 26.286C25.1847 26.286 25.5687 26.138 25.8607 25.846C26.4467 25.26 26.4467 24.31 25.8607 23.724L22.1127 19.978L25.8387 16.25C26.4247 15.664 26.4247 14.716 25.8387 14.13C25.2527 13.544 24.3047 13.544 23.7187 14.13L19.9907 17.858L16.2587 14.126C15.6727 13.54 14.7247 13.54 14.1387 14.126C13.5527 14.712 13.5527 15.66 14.1387 16.246L17.8707 19.978L14.1387 23.714C13.5527 24.3 13.5527 25.248 14.1387 25.834C14.4307 26.128 14.8167 26.274 15.1987 26.274C15.5827 26.274 15.9667 26.128 16.2587 25.834L19.9927 22.1L23.7387 25.846C24.0327 26.138 24.4147 26.286 24.8007 26.286Z"
                                fill="#D92D20" />
                        </svg>
                    </a>
                </div>
            </div>

            <div class="line-faq container hidden" id="line-faq-1"></div>
            <div class="answer container hidden" id="answer-1">
                <p>Untuk memulainya kamu bisa menggunakan fitur BMI untuk mengetahui kategori berat badanmu,
                    memperkirakan jumlah lemak tubuh, dan menentukan apakah kamu berisiko mengalami penyakit kronis</p>
            </div>
        </div>

        <div class="accordion1 container">
            <div class="accordion-header">
                <p>fitur apa saja yang bisa dinikmati di website educate?</p>
                <div class="accordion-icon" onclick="toggleAccordion(2)">
                    <a href="#faq1" id="accordion-link-2">
                        <svg class="plus-svg" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4"
                                d="M29.3333 0H10.6667C3.85778 0 0 3.85778 0 10.6667V29.3333C0 36.1244 3.84 40 10.6667 40H29.3333C36.1422 40 40 36.1244 40 29.3333V10.6667C40 3.85778 36.1422 0 29.3333 0Z"
                                fill="#5837D0" />
                            <path
                                d="M26.6409 21.4167H21.499V26.514C21.499 27.3347 20.8279 28 20 28C19.1721 28 18.501 27.3347 18.501 26.514V21.4167H13.3591C12.5868 21.3374 12 20.6923 12 19.9226C12 19.153 12.5868 18.5079 13.3591 18.4286H18.4848V13.3473C18.5648 12.5818 19.2156 12 19.9919 12C20.7683 12 21.4191 12.5818 21.499 13.3473V18.4286H26.6409C27.4132 18.5079 28 19.153 28 19.9226C28 20.6923 27.4132 21.3374 26.6409 21.4167V21.4167Z"
                                fill="#5837D0" />
                        </svg>
                        <svg class="close-svg hidden" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                                d="M20 0.5C5.604 0.5 0.5 5.604 0.5 20C0.5 34.396 5.604 39.5 20 39.5C34.398 39.5 39.5 34.396 39.5 20C39.5 5.604 34.398 0.5 20 0.5Z"
                                fill="#D92D20" />
                            <path
                                d="M24.8007 26.286C25.1847 26.286 25.5687 26.138 25.8607 25.846C26.4467 25.26 26.4467 24.31 25.8607 23.724L22.1127 19.978L25.8387 16.25C26.4247 15.664 26.4247 14.716 25.8387 14.13C25.2527 13.544 24.3047 13.544 23.7187 14.13L19.9907 17.858L16.2587 14.126C15.6727 13.54 14.7247 13.54 14.1387 14.126C13.5527 14.712 13.5527 15.66 14.1387 16.246L17.8707 19.978L14.1387 23.714C13.5527 24.3 13.5527 25.248 14.1387 25.834C14.4307 26.128 14.8167 26.274 15.1987 26.274C15.5827 26.274 15.9667 26.128 16.2587 25.834L19.9927 22.1L23.7387 25.846C24.0327 26.138 24.4147 26.286 24.8007 26.286Z"
                                fill="#D92D20" />
                        </svg>
                    </a>
                </div>
            </div>

            <div class="line-faq container hidden" id="line-faq-2"></div>
            <div class="answer container hidden" id="answer-2">
                <p>ada banyak fitur yang bisa kamu nikmati seperti BMI calculator, konsultasi, article resep makanan,
                    dan juga komunitas</p>
            </div>
        </div>

        <div class="accordion1 container">
            <div class="accordion-header">
                <p>bagaimana cara melakukan konsultasi dengan dokter di educate</p>
                <div class="accordion-icon" onclick="toggleAccordion(3)">
                    <a href="#faq1" id="accordion-link-3">
                        <svg class="plus-svg" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4"
                                d="M29.3333 0H10.6667C3.85778 0 0 3.85778 0 10.6667V29.3333C0 36.1244 3.84 40 10.6667 40H29.3333C36.1422 40 40 36.1244 40 29.3333V10.6667C40 3.85778 36.1422 0 29.3333 0Z"
                                fill="#5837D0" />
                            <path
                                d="M26.6409 21.4167H21.499V26.514C21.499 27.3347 20.8279 28 20 28C19.1721 28 18.501 27.3347 18.501 26.514V21.4167H13.3591C12.5868 21.3374 12 20.6923 12 19.9226C12 19.153 12.5868 18.5079 13.3591 18.4286H18.4848V13.3473C18.5648 12.5818 19.2156 12 19.9919 12C20.7683 12 21.4191 12.5818 21.499 13.3473V18.4286H26.6409C27.4132 18.5079 28 19.153 28 19.9226C28 20.6923 27.4132 21.3374 26.6409 21.4167V21.4167Z"
                                fill="#5837D0" />
                        </svg>
                        <svg class="close-svg hidden" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                                d="M20 0.5C5.604 0.5 0.5 5.604 0.5 20C0.5 34.396 5.604 39.5 20 39.5C34.398 39.5 39.5 34.396 39.5 20C39.5 5.604 34.398 0.5 20 0.5Z"
                                fill="#D92D20" />
                            <path
                                d="M24.8007 26.286C25.1847 26.286 25.5687 26.138 25.8607 25.846C26.4467 25.26 26.4467 24.31 25.8607 23.724L22.1127 19.978L25.8387 16.25C26.4247 15.664 26.4247 14.716 25.8387 14.13C25.2527 13.544 24.3047 13.544 23.7187 14.13L19.9907 17.858L16.2587 14.126C15.6727 13.54 14.7247 13.54 14.1387 14.126C13.5527 14.712 13.5527 15.66 14.1387 16.246L17.8707 19.978L14.1387 23.714C13.5527 24.3 13.5527 25.248 14.1387 25.834C14.4307 26.128 14.8167 26.274 15.1987 26.274C15.5827 26.274 15.9667 26.128 16.2587 25.834L19.9927 22.1L23.7387 25.846C24.0327 26.138 24.4147 26.286 24.8007 26.286Z"
                                fill="#D92D20" />
                        </svg>
                    </a>
                </div>
            </div>

            <div class="line-faq container hidden" id="line-faq-3"></div>
            <div class="answer container hidden" id="answer-3">
                <p>kamu bisa melakukan konsultasi dengan dokter gizi yang ada di eatducate melalui menu konsultasi</p>
            </div>
        </div>

        <div class="accordion1 container">
            <div class="accordion-header">
                <p>Apakah fitur komunitas dapat membantu saya semakin giat untuk diet?</p>
                <div class="accordion-icon" onclick="toggleAccordion(4)">
                    <a href="#faq1" id="accordion-link-4">
                        <svg class="plus-svg" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4"
                                d="M29.3333 0H10.6667C3.85778 0 0 3.85778 0 10.6667V29.3333C0 36.1244 3.84 40 10.6667 40H29.3333C36.1422 40 40 36.1244 40 29.3333V10.6667C40 3.85778 36.1422 0 29.3333 0Z"
                                fill="#5837D0" />
                            <path
                                d="M26.6409 21.4167H21.499V26.514C21.499 27.3347 20.8279 28 20 28C19.1721 28 18.501 27.3347 18.501 26.514V21.4167H13.3591C12.5868 21.3374 12 20.6923 12 19.9226C12 19.153 12.5868 18.5079 13.3591 18.4286H18.4848V13.3473C18.5648 12.5818 19.2156 12 19.9919 12C20.7683 12 21.4191 12.5818 21.499 13.3473V18.4286H26.6409C27.4132 18.5079 28 19.153 28 19.9226C28 20.6923 27.4132 21.3374 26.6409 21.4167V21.4167Z"
                                fill="#5837D0" />
                        </svg>
                        <svg class="close-svg hidden" xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                            viewBox="0 0 40 40" fill="none">
                            <path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                                d="M20 0.5C5.604 0.5 0.5 5.604 0.5 20C0.5 34.396 5.604 39.5 20 39.5C34.398 39.5 39.5 34.396 39.5 20C39.5 5.604 34.398 0.5 20 0.5Z"
                                fill="#D92D20" />
                            <path
                                d="M24.8007 26.286C25.1847 26.286 25.5687 26.138 25.8607 25.846C26.4467 25.26 26.4467 24.31 25.8607 23.724L22.1127 19.978L25.8387 16.25C26.4247 15.664 26.4247 14.716 25.8387 14.13C25.2527 13.544 24.3047 13.544 23.7187 14.13L19.9907 17.858L16.2587 14.126C15.6727 13.54 14.7247 13.54 14.1387 14.126C13.5527 14.712 13.5527 15.66 14.1387 16.246L17.8707 19.978L14.1387 23.714C13.5527 24.3 13.5527 25.248 14.1387 25.834C14.4307 26.128 14.8167 26.274 15.1987 26.274C15.5827 26.274 15.9667 26.128 16.2587 25.834L19.9927 22.1L23.7387 25.846C24.0327 26.138 24.4147 26.286 24.8007 26.286Z"
                                fill="#D92D20" />
                        </svg>
                    </a>
                </div>
            </div>

            <div class="line-faq container hidden" id="line-faq-4"></div>
            <div class="answer container hidden" id="answer-4">
                <p>tentu saja , dengan adanya fitur komunitas kamu bisa melakukan hal-hal yang susah menjadi mudah</p>
            </div>
        </div>

        <script>
            function toggleAccordion(accordionNumber) {
                const lineFaq = document.getElementById('line-faq-' + accordionNumber);
                const answer = document.getElementById('answer-' + accordionNumber);
                const accordionLink = document.getElementById('accordion-link-' + accordionNumber);

                // Toggle class to show/hide elements
                lineFaq.classList.toggle('hidden');
                answer.classList.toggle('hidden');

                // Toggle class for SVG change
                const plusSvg = accordionLink.querySelector('.plus-svg');
                const closeSvg = accordionLink.querySelector('.close-svg');

                if (plusSvg && closeSvg) {
                    plusSvg.classList.toggle('hidden');
                    closeSvg.classList.toggle('hidden');
                }
            }
        </script>
    </div>
    <!-- End FAQ -->
@endsection
