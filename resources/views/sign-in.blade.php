<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/sign-in.css') }}" />
    <title>Sign-In</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-6 kolom-kiri">
                <div class="navigation">
                    <div class="logo">
                        <a class="navbar-brand" href="/">
                            <img src="{{ asset('assets/logo/Logo.png') }}" alt="Logo" width="auto" height="28"
                                class="d-inline-block align-text-top">
                            <span class="edu">Eatducate</span>
                        </a>
                    </div>
                    <div class="buttons">
                        <div class="back">
                            <a class="btn btn-primary" href="/" role="button">
                                <svg xmlns="http://www.w3.org/2000/svg" width="19" height="12" viewBox="0 0 19 12"
                                    fill="none">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M8.68846 0.911691C9.22446 1.44669 9.40945 3.25869 9.45945 5.00369L9.46545 7.00369C9.41245 9.24069 9.15245 10.6107 8.68845 11.0747C8.12445 11.6377 7.18445 11.3487 6.42845 11.0347C4.85945 10.3837 0.323454 7.63769 0.323454 5.99269C0.323454 4.29769 5.06146 1.56069 6.50046 0.963691C7.28246 0.638691 8.15346 0.378691 8.68846 0.911691Z"
                                        fill="#1C7C54" />
                                    <path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                                        d="M9.45939 5.00391L17.6764 5.00391C18.2294 5.00391 18.6764 5.45091 18.6764 6.00391C18.6764 6.55691 18.2294 7.00391 17.6764 7.00391L9.46539 7.00391L9.45939 5.00391Z"
                                        fill="#1C7C54" />
                                </svg> Go Home
                            </a>
                        </div>
                    </div>
                </div>

                <p class="sig-in">sign in</p>

                <div class="form">

                    <form action="/sign-in" method="post">
                        @csrf
                        <div class="mb-3">
                            <label for="email" class="form-label">Email<span class="bintang">*</span></label>
                            <input class="form-control lowercase" type="email" name="email" placeholder="Enter Your Email"
                                aria-label="default input example" required>
                            @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="password" class="form-label">Password<span class="bintang">*</span></label>
                            <input class="form-control lowercase" type="password" name="password"
                                placeholder="Create Your Password" aria-label="default input example" required>
                            @error('password')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="tombol_sign-in">
                            <button class="btn btn-primary" type="submit">Sign In</button>
                        </div>
                    </form>
                    <div class="account">
                        <p>Don't have an acount? <a href="/sign-up" class="biru">sign Up</a></p>
                    </div>
                </div>
            </div>

            <div class="col-6 kolom-kanan">

            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"
        integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous">
    </script>
</body>
</html>
