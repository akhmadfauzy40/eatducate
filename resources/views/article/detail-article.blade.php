@extends('layouts.master')

@section('title')
    Article
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/detailArticle.css') }}" />
@endpush

@section('content')
    <!-- Start Breadcrumb -->
    <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);"
        aria-label="breadcrumb">
        <ol class="breadcrumb container">
            <li class="breadcrumb-item"><a href="/article">Artikel</a></li>
            <li class="breadcrumb-item active" aria-current="page">Read Artikel</li>
        </ol>
    </nav>
    <!-- Start Breadcrumb -->

    <!-- Start Detail-Article -->
    <div class="title-detailArticle container text-center">
        <p>Menemukan Kebahagiaan dalam Olahraga: Motivasi Diet Sehat</p>
        <p class="account">mora</p>
        <p class="tanggal">19 Desember 2023</p>
    </div>
    <!-- End Detail-Article -->

    <!-- Start Hero -->
    <div class="hero container"></div>
    <!-- End Hero -->

    <!-- Start text-detailArticle -->
    <div class="text-detailArticle container">
        <p class="explanation">
            Kesadaran akan pentingnya pola makan sehat semakin mendapatkan tempat istimewa di tengah masyarakat.
            Terlebih lagi, tren makanan sehat yang juga lezat kini semakin memikat lidah para pecinta kuliner. Restoran
            dan kafe yang menawarkan menu sehat dengan citarasa yang menggugah selera menjadi favorit bagi mereka yang
            ingin menjaga kesehatan tanpa mengorbankan kenikmatan kuliner. Berbagai hidangan kreatif dan inovatif,
            seperti salad segar dengan saus organik, smoothie bowl dengan topping beragam, dan hidangan utama rendah
            lemak namun kaya akan rasa, semakin banyak dicari sebagai alternatif makanan sehat yang tak kalah enak.
        </p>
        <p class="subjudul">Inovasi Makanan Sehat yang Memanjakan Lidah</p>
        <p class="explanation">
            Industri makanan terus berinovasi dengan menciptakan hidangan sehat yang tak hanya memberikan nutrisi,
            tetapi juga memanjakan lidah. Makanan superfood, seperti quinoa, kale, dan chia seeds, menjadi bahan utama
            dalam berbagai kreasi kuliner yang disajikan di berbagai tempat makan. Tak hanya itu, koki-koki handal juga
            semakin kreatif dalam meramu bumbu dan rempah, menciptakan hidangan sehat dengan sentuhan rasa yang unik dan
            lezat. Kombinasi antara cita rasa yang enak dan nilai gizi yang tinggi membuat makanan sehat tidak lagi
            terasa monoton, melainkan menjadi pilihan yang menggairahkan selera.
        </p>
        <p class="subjudul">Makanan Sehat sebagai Gayaberhasil di Era Modern</p>
        <p class="explanation">Dalam era modern ini, gaya hidup sehat bukan lagi sekadar tren, tetapi menjadi kebutuhan.
            Masyarakat semakin
            sadar akan pentingnya menjaga kesehatan melalui pola makan yang tepat. Restoran dan kafe yang mengusung
            konsep makanan sehat tidak hanya menyasar kalangan fitness enthusiast, tetapi juga mereka yang ingin
            menjalani gaya hidup seimbang. Makanan sehat dan enak bukan lagi sesuatu yang sulit diakses, melainkan telah
            menjadi bagian dari gaya hidup masyarakat perkotaan yang ingin menjaga kesehatan tanpa mengorbankan
            kenikmatan kuliner
        </p>
    </div>
    <!-- End text-detailArticle -->
@endsection
