@extends('layouts.master')

@section('title')
    Article
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/article.css') }}" />
@endpush

@section('content')
    <!-- Start Hero -->
    <div class="hero container-fluid"></div>
    <!-- End Hero -->

    <!-- Start Article -->
    <div class="article container">
        <p class="title-article text-center">our recent Article</p>
    </div>

    <div class="card-article container mb-5">
        <div class="row row-cols-1 row-cols-md-3 g-4">
            <div class="col">
                <a href="detail-article" class="link-article">
                    <div class="card h-100">
                        <img src="{{ asset('assets/ilustration/Article1.jpeg') }}" class="card-img-top" alt="Ilustration"
                            loading="lazy">
                        <div class="card-body">
                            <h5 class="card-title">Menemukan Kebahagiaan dalam Olahraga: Motivasi Diet Sehat</h5>
                            <p class="card-text">Artikel ini membahas keterkaitan antara kebahagiaan, olahraga, dan
                                motivasi
                                untuk menjalani diet sehat</p>
                            <div class="profile-card d-flex flex-row">
                                <div class="gambar">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                                        viewBox="0 0 48 48" fill="none">
                                        <circle cx="24" cy="24" r="24" fill="#98A2B3" />
                                    </svg>
                                </div>

                                <div class="detail-card">
                                    <p class="account">Mora</p>
                                    <p class="tanggal">19 Desember 2023</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col">
                <a href="/detail-article" class="link-article">
                    <div class="card h-100">
                        <img src="{{ asset('assets/ilustration/Article2.jpeg') }}" class="card-img-top" alt="Ilustration"
                            loading="lazy">
                        <div class="card-body">
                            <h5 class="card-title">Transformasi Hidup: Motivasi Tersembunyi dalam Program Diet</h5>
                            <p class="card-text">Mengungkap motivasi tersembunyi yang bisa merubah hidup melalui kisah
                                sukses dalam menjalani program diet.</p>
                            <div class="profile-card d-flex flex-row">
                                <div class="gambar">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                                        viewBox="0 0 48 48" fill="none">
                                        <circle cx="24" cy="24" r="24" fill="#98A2B3" />
                                    </svg>
                                </div>

                                <div class="detail-card">
                                    <p class="account">Mora</p>
                                    <p class="tanggal">19 Desember 2023</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col">
                <a href="/detail-article" class="link-article">
                    <div class="card h-100">
                        <img src="{{ asset('assets/ilustration/Article3.jpeg') }}" class="card-img-top" alt="Ilustration"
                            loading="lazy">
                        <div class="card-body">
                            <h5 class="card-title">Diet Paleo: Makanan Alami untuk Tubuh Sehat Optimal
                            </h5>
                            <p class="card-text">Diet Paleolitik, Paleo diet, diet manusia gua, atau diet
                                zaman batuadalah sebuah mode diet modern yang mensyaratkan makan yang dikonsumsi
                                hanya makanan yang dianggap telah tersedia bagi manusia pada era Paleolitikum</p>
                            <div class="profile-card d-flex flex-row">
                                <div class="gambar">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                                        viewBox="0 0 48 48" fill="none">
                                        <circle cx="24" cy="24" r="24" fill="#98A2B3" />
                                    </svg>
                                </div>

                                <div class="detail-card">
                                    <p class="account">Mora</p>
                                    <p class="tanggal">19 Desember 2023</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col">
                <a href="/detail-article" class="link-article">
                    <div class="card h-100">
                        <img src="{{ asset('assets/ilustration/Article4.jpeg') }}" class="card-img-top" alt="Ilustration"
                            loading="lazy">
                        <div class="card-body">
                            <h5 class="card-title">Makanan Penurun Stres: Kunci Sukses Diet Sehat</h5>
                            <p class="card-text">Mengulas makanan-makanan yang dapat membantu menurunkan stres dan
                                mendukung kesuksesan diet sehat</p>
                            <div class="profile-card d-flex flex-row">
                                <div class="gambar">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                                        viewBox="0 0 48 48" fill="none">
                                        <circle cx="24" cy="24" r="24" fill="#98A2B3" />
                                    </svg>
                                </div>

                                <div class="detail-card">
                                    <p class="account">Mora</p>
                                    <p class="tanggal">19 Desember 2023</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col">
                <a href="/detail-article" class="link-article">
                    <div class="card h-100">
                        <img src="{{ asset('assets/ilustration/Article5.jpeg') }}" class="card-img-top" alt="Ilustration"
                            loading="lazy">
                        <div class="card-body">
                            <h5 class="card-title">Resep Sederhana, Hidup Sehat: Motivasi Dapur untuk Diet</h5>
                            <p class="card-text">Memberikan resep-resep sederhana sebagai motivasi dalam memulai dan
                                menjalani gaya hidup sehat melalui diet.</p>
                            <div class="profile-card d-flex flex-row">
                                <div class="gambar">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                                        viewBox="0 0 48 48" fill="none">
                                        <circle cx="24" cy="24" r="24" fill="#98A2B3" />
                                    </svg>
                                </div>

                                <div class="detail-card">
                                    <p class="account">Mora</p>
                                    <p class="tanggal">19 Desember 2023</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col">
                <a href="/detail-article" class="link-article">
                    <div class="card h-100">
                        <img src="{{ asset('assets/ilustration/Article2.jpeg') }}" class="card-img-top" alt="Ilustration"
                            loading="lazy">
                        <div class="card-body">
                            <h5 class="card-title">Pentingnya Sarapan: Motivasi Awal Hari dalam Diet</h5>
                            <p class="card-text">Menyoroti pentingnya sarapan sebagai motivasi awal hari untuk menjalani
                                diet sehat</p>
                            <div class="profile-card d-flex flex-row">
                                <div class="gambar">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                                        viewBox="0 0 48 48" fill="none">
                                        <circle cx="24" cy="24" r="24" fill="#98A2B3" />
                                    </svg>
                                </div>

                                <div class="detail-card">
                                    <p class="account">Mora</p>
                                    <p class="tanggal">19 Desember 2023</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col">
                <a href="/detail-article" class="link-article">
                    <div class="card h-100">
                        <img src="{{ asset('assets/ilustration/Article4.jpeg') }}" class="card-img-top" alt="Ilustration"
                            loading="lazy">
                        <div class="card-body">
                            <h5 class="card-title">Makanan Penurun Stres: Kunci Sukses Diet Sehat</h5>
                            <p class="card-text">Mengulas makanan-makanan yang dapat membantu menurunkan stres dan
                                mendukung kesuksesan diet sehat</p>
                            <div class="profile-card d-flex flex-row">
                                <div class="gambar">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                                        viewBox="0 0 48 48" fill="none">
                                        <circle cx="24" cy="24" r="24" fill="#98A2B3" />
                                    </svg>
                                </div>

                                <div class="detail-card">
                                    <p class="account">Mora</p>
                                    <p class="tanggal">19 Desember 2023</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col">
                <a href="/detail-article" class="link-article">
                    <div class="card h-100">
                        <img src="{{ asset('assets/ilustration/Article5.jpeg') }}" class="card-img-top" alt="Ilustration"
                            loading="lazy">
                        <div class="card-body">
                            <h5 class="card-title">Resep Sederhana, Hidup Sehat: Motivasi Dapur untuk Diet</h5>
                            <p class="card-text">Memberikan resep-resep sederhana sebagai motivasi dalam memulai dan
                                menjalani gaya hidup sehat melalui diet.</p>
                            <div class="profile-card d-flex flex-row">
                                <div class="gambar">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                                        viewBox="0 0 48 48" fill="none">
                                        <circle cx="24" cy="24" r="24" fill="#98A2B3" />
                                    </svg>
                                </div>

                                <div class="detail-card">
                                    <p class="account">Mora</p>
                                    <p class="tanggal">19 Desember 2023</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col">
                <a href="/detail-article" class="link-article">
                    <div class="card h-100">
                        <img src="{{ asset('assets/ilustration/Article2.jpeg') }}" class="card-img-top" alt="Ilustration"
                            loading="lazy">
                        <div class="card-body">
                            <h5 class="card-title">Pentingnya Sarapan: Motivasi Awal Hari dalam Diet</h5>
                            <p class="card-text">Menyoroti pentingnya sarapan sebagai motivasi awal hari untuk menjalani
                                diet sehat</p>
                            <div class="profile-card d-flex flex-row">
                                <div class="gambar">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                                        viewBox="0 0 48 48" fill="none">
                                        <circle cx="24" cy="24" r="24" fill="#98A2B3" />
                                    </svg>
                                </div>

                                <div class="detail-card">
                                    <p class="account">Mora</p>
                                    <p class="tanggal">19 Desember 2023</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <!-- End Article -->
@endsection
