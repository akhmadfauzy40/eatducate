@extends('layouts.master')

@section('title')
    BMI Results
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/result.css') }}" />
@endpush

@section('content')
    <!-- Start Breadcrumb -->
    <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);"
        aria-label="breadcrumb">
        <ol class="breadcrumb container">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/#bmi">BMI</a></li>
            <li class="breadcrumb-item active" aria-current="page">BMI Result</li>
        </ol>
    </nav>
    <!-- Start Breadcrumb -->

    <!-- Start BMI -->
    <div class="bmi container">
        <div class="score">
            <p class="bmi-score" style="color: {{ $bmi < 18.5 ? '#175cd3' : ($bmi < 25 ? '#1c7c54' : '#f04438') }}">{{ $bmi }}</p>
            <p class="cat-score">{{ $catScore }}</p>
            <p class="desc-score">{{ $descScore }}</p>
        </div>
        <div class="diagram container text-center">
            <div class="row align-items-start">
                <div class="col">
                    <p class="cat-diagram">underweight</p>
                </div>
                <div class="col">
                    <p class="cat-diagram">Normal</p>
                </div>
                <div class="col">
                    <p class="cat-diagram">overweight</p>
                </div>
            </div>
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-label="Segment one" style="width: 33.3333333333%"
                    aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-success" role="progressbar" aria-label="Segment two"
                    style="width: 33.3333333333%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                <div class="progress-bar bg-danger" role="progressbar" aria-label="Segment three"
                    style="width: 33.3333333333%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="index">
                <div class="container text-center">
                    <div class="row align-items-start">
                        <div class="col">
                            <div class="index-double d-flex justify-content-between">
                                <p class="left">0</p>
                                <p class="right">18.5</p>
                            </div>
                        </div>
                        <div class="col">
                            <div class="index-tengah text-end">
                                <p>25</p>
                            </div>
                        </div>
                        <div class="col">
                            <div class="index-kanan text-end">
                                <p>40</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start Informasi -->
        <div class="informasi">
            <div class="judul-informasi d-flex">
                <p class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34"
                        fill="none">
                        <path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                            d="M17 0.75C5.00333 0.75 0.75 5.00333 0.75 17C0.75 28.9967 5.00333 33.25 17 33.25C28.9967 33.25 33.25 28.9967 33.25 17C33.25 5.00333 28.9967 0.75 17 0.75Z"
                            fill="#175CD3" />
                        <path
                            d="M16.9937 12.4167H17.0087C17.6987 12.4167 18.252 11.8567 18.252 11.1667C18.252 10.4767 17.6837 9.91675 16.9937 9.91675C16.3037 9.91675 15.7437 10.4767 15.7437 11.1667C15.7437 11.8567 16.3037 12.4167 16.9937 12.4167Z"
                            fill="#175CD3" />
                        <path
                            d="M17.0003 24.7417C17.6903 24.7417 18.2503 24.1817 18.2503 23.4917V17.0001C18.2503 16.3101 17.6903 15.7501 17.0003 15.7501C16.3103 15.7501 15.7503 16.3101 15.7503 17.0001V23.4917C15.7503 24.1817 16.3103 24.7417 17.0003 24.7417Z"
                            fill="#175CD3" />
                    </svg></p>
                <p class="text">Informasi</p>
            </div>
            <p class="desc-informasi">
                BMI tidak sepenuhnya mewakili diagnosis menyeluruh dari kesehatan tubuh dan resiko penyakit seseorang.
                Anda perlu konsultasi lebih lanjut mengenai resiko dan kekhawatiran Anda terkait dengan berat badan
                Anda.
            </p>
        </div>
        <!-- End Informasi -->

        <!-- Start Risiko -->
        <div class="risiko">
            <div class="judul-informasi d-flex">
                <p class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="34" height="32" viewBox="0 0 34 32"
                        fill="none">
                        <path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd"
                            d="M28.767 12.9301L28.4136 12.3067C23.437 3.45675 20.3436 0.166748 17.0003 0.166748C13.657 0.166748 10.5636 3.45675 5.58696 12.3067L5.2353 12.9301C3.44696 16.0667 -0.269702 22.5917 0.420298 26.4851C1.23363 31.0901 5.9753 31.8334 17.0003 31.8334C28.027 31.8334 32.7686 31.0901 33.5803 26.4851C34.2703 22.5917 30.5536 16.0667 28.767 12.9301Z"
                            fill="#F79009" />
                        <path
                            d="M15.7512 22.4917C15.7512 23.1817 16.3178 23.7417 17.0078 23.7417C17.6978 23.7417 18.2578 23.1817 18.2578 22.4917C18.2578 21.8017 17.6978 21.2417 17.0078 21.2417H16.9928C16.3028 21.2417 15.7512 21.8017 15.7512 22.4917Z"
                            fill="#F79009" />
                        <path
                            d="M16.9995 8.91675C16.3095 8.91675 15.7495 9.47675 15.7495 10.1667V16.6584C15.7495 17.3484 16.3095 17.9084 16.9995 17.9084C17.6895 17.9084 18.2495 17.3484 18.2495 16.6584V10.1667C18.2495 9.47675 17.6895 8.91675 16.9995 8.91675Z"
                            fill="#F79009" />
                    </svg></p>
                <p class="text">risiko</p>
            </div>
            <p class="desc-risiko">
                Orang dengan nilai indeks massa tubuh normal tetap mungkin berisiko mengalami penyakit tertentu. Namun
                risikonya lebih rendah ketimbang orang dengan nilai IMT yang tidak normal. Orang dengan IMT normal juga
                bisa memiliki persen lemak yang tinggi, maka itu harus dihitung lemak tubuh dan kondisi metabolismenya.
                Supaya bisa mencegah beragam penyakit kronis, penting untuk menjaga pola hidup sehat, mulai dari pola
                makan yang baik, olahraga, serta pemeriksaan kesehatan secara rutin.
            </p>
        </div>
        <!-- End Risiko -->

        <!-- Start Tombol -->
        <div class="tombol">
            <a class="btn btn-success" href="/#bmi" role="button">Hitung ulang</a>
        </div>
        <!-- End Tombol -->
    </div>
    <!-- End BMI -->
@endsection
