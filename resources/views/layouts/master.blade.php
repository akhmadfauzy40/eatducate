<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    @stack('styles')
    <title>@yield('title')</title>
</head>

<body>
    <!-- Start Navbar -->
    <nav class="navbar bg-light">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="{{ asset('assets/logo/Logo.png') }}" alt="Logo" width="auto" height="28"
                    class="d-inline-block align-text-top" loading="lazy">
                <span>Eatducate</span>
            </a>

            <ul class="nav justify-content-end">
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/article">Article</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/community">Community</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/chatbot">Chatbot</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/consultation">Consultation</a>
                </li>
                @auth
                    <li class="nav-item">
                        <div class="profile">
                            <button type="button" class="btn btn-secondary" data-bs-container="body"
                                data-bs-toggle="popover" data-bs-placement="bottom" data-bs-html="true"
                                data-bs-content='<a href="/logout">Logout</a>'>
                                {{ substr(Auth::user()->name, 0, 2) }}
                            </button>
                        </div>
                    </li>
                @endauth
                @guest
                    <li class="nav-item">
                        <a class="btn btn-primary sign-in" href="/sign-in" role="button">Sign in</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-outline-success sign-up" href="/sign-up" role="button">Sign up</a>
                    </li>
                @endguest
            </ul>
        </div>
    </nav>
    <!-- End Navbar -->

    @yield('content')

    <!-- Start Footer -->
    <div class="footer container-fluid">
        <div class="navigation-footer container">
            <a href="/">Home</a>
            <a href="/article">article</a>
            <a href="/community">community</a>
            <a href="/chatbot">chatbot</a>
            <a href="/consultation">Consultation</a>
            <a href="/faq">FAQ</a>
            <a href="/about">About</a>
        </div>
        <div class="line container"></div>
        <div class="copyright">
            <p>@2024 copyright eatducate</p>
        </div>
    </div>
    <!-- End Footer -->

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"
        integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous">
    </script>
    <script>
        const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
        const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
    </script>

    @stack('scripts')
</body>

</html>
