## Tutor Instalasi Web

- buat folder baru lalu buka terminal di folder tersebut lalu ketik perintah di bawah ini

```bash
git clone https://gitlab.com/akhmadfauzy40/eatducate.git
```

```bash
cd eatducate
```

```bash
composer install
```

- buat file baru dengan nama ".env" lalu copy isi dari file ".env.example" dan paste ke dalam ".env"
- ubah value dari DB_DATABASE menjadi "eatducate"
- lalu jalankan perintah di bawah ini

```bash
php artisan key:generate
```

```bash
php artisan migrate
```

- ketik yes lalu enter

```bash
php artisan serve
```

