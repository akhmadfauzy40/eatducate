<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BMIController extends Controller
{
    public function calculate(Request $request)
    {
        $request->validate([
            'height' => 'required|numeric|min:1',
            'weight' => 'required|numeric|min:1',
        ]);

        $height = $request->input('height') / 100; // Konversi tinggi dari cm ke meter
        $weight = $request->input('weight');

        $bmi = round($weight / ($height * $height), 2);

        $catScore = $this->getBmiCatScore($bmi);
        $descScore = $this->getBmiDescScore($bmi);
        return view('result', ['bmi' => $bmi, 'catScore' => $catScore, 'descScore' => $descScore]);
    }

    private function getBmiCatScore($value)
    {
        if ($value < 18.5) {
            return 'Kurus';
        } elseif ($value < 25) {
            return 'Normal';
        } elseif ($value < 30) {
            return 'Kegemukan';
        } else {
            return 'Obesitas';
        }
    }

    private function getBmiDescScore($value)
    {
        if ($value < 18.5) {
            return 'Berat badan kurang';
        } elseif ($value < 25) {
            return 'Berat badan normal';
        } elseif ($value < 30) {
            return 'Berat badan berlebih';
        } else {
            return 'Obesitas';
        }
    }
}
