<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BMIController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/sign-in', function () {
    return view('sign-in');
})->name('login');

Route::get('/sign-up', function () {
    return view('sign-up');
});

Route::get('/result', function () {
    return view('result');
})->name('result');

Route::get('/faq', function () {
    return view('faq');
});

Route::get('/community', function () {
    return view('community.index');
});

Route::get('/community-detail', function () {
    return view('community.community-detail');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/article', function () {
    return view('article.index');
});

Route::get('/detail-article', function () {
    return view('article.detail-article');
});

Route::get('/detail-consultation', function () {
    return view('consultation.consultation-detail');
});
Route::get('/checkout-keluhan', function () {
    return view('consultation.checkout-keluhan');
});
Route::get('/checkout-metode-konsultasi', function () {
    return view('consultation.checkout-metode');
});
Route::get('/metode-pembayaran', function () {
    return view('consultation.checkout-pembayaran');
});
Route::get('/detail-pembayaran', function () {
    return view('consultation.detail-pembayaran');
});

Route::post('/sign-up/store', [AuthController::class, 'store']);
Route::post('/sign-in', [AuthController::class, 'login']);
Route::get('/logout', [AuthController::class, 'logout']);
Route::post('/calculate-bmi', [BMIController::class, 'calculate']);

Route::middleware('auth')->group(function(){
    Route::get('/chatbot', function () {
        return view('chatbot');
    });
    Route::get('/consultation', function () {
        return view('consultation.index');
    });
});
